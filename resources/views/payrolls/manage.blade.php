@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Payroll </h1>
            <!-- end page-header -->
            <!-- begin panel -->
   <div class="row">
   <div class="col-md-5">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Profile</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
	           
	                     <div class="profile-view">
	                        <div class="profile-img-wrap">
	                           <div class="profile-img">
							   @if ($user->employee->avatar)
							   
							   	<img src="{{ Storage::url($user->employee->avatar) }}" alt="" style="height: 40%; width: 70%"> 
							    @else
								<img src="{{ asset('img/admin.png') }}" alt=""  style="height: 50%; width: 50%">
								@endif
	                           </div>
	                        </div>
			                        <div class="profile-basic">
			                           <div class="row">
			                              <div class="col-md-5">
			                                 <div class="profile-info-left">
			                                 	<br>
			                                    <h3 class="user-name m-t-0 mb-0">{{ $user->employee->name ?? null }}</h3>
			                                    <h4 class="text-muted">{{ $user->employee->user->role->email ?? null }}</h4>
			                                    <div class="staff-id"><h4>Employee ID : {{ $user->employee->employee_no ?? null }}</h4></div>
			                                    <div class="small doj text-muted"><h4>Date of Join : {{ $user->employee->joining_date ?? null }}</h4></div>
			                                 </div>
			                              </div>
			                              <div class="col-md-7">
			                              	<br>
			                                 <ul class="personal-info">
			                                    <li>
			                                       <div class="title"><h4>Phone:</h4></div>
			                                       <div class="text"><h4><a href="">{{ $user->employee->phone ?? null }}</a></h4></div>
			                                    </li>
			                                    <li>
			                                       <div class="title"><h4>Email:</h4></div>
			                                       <div class="text"><h4><a href=""><span class="_cf_email_" data-cfemail="c1abaea9afa5aea481a4b9a0acb1ada4efa2aeac">{{ $user->employee->user->email ?? NULL }}</span></h4></a></div>
			                                    </li>
			                                    <li>
			                                       <div class="title"><h4>Birthday:</h4></div>
			                                       <div class="text"><h4>{{ $user->employee->birthday ?? null }}</h4></div>
			                                    </li>
			                                    <li>
			                                       <div class="title"><h4>Address:</h4></div>
			                                       <div class="text"><h4>{{ $user->employee->address ?? null }}</h4></div>
			                                    </li>
			                                    <li>
			                                       <div class="title"><h4>Gender:</h4></div>
			                                       <div class="text"><h4>{{ $user->employee->gender ?? null }}</h4></div>
			                                    </li> 
			                              </div>
			                              </li>
			                              </ul>
			                           </div>
			                        </div>
	                     </div>
	                     
	                  </div>
	               </div>
	            </div>
	     
 <div class="col-md-7">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Attendance</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
      	<div class="form-group">
      		<a href="/attendance-report/{{ $user->id }}" class="btn btn-primary" target="_blank">Generate PDF</a>
      	</div>
			<table class="table table-bordered">
			<thead>
			<tr>
			<th>Date</th>
			<th>Time In</th>
			<th>Time Out</th>
			<th>Total Hours</th> 
			</tr>
			</thead>
			<tbody>
				@php 
				 	$sumTotalHours = 0;
				@endphp
				@foreach ($user->attendances as $attendance)
					@php
						$sumTotalHours  += $attendance['total'];
						$sumTotalPayrolls = $sumTotalHours * $user->employee->rates;
					@endphp
				<tr>
					<td>{{ $attendance->date }}</td>
					<td>{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</td>
					<td> {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}</td>
					<td>{{ $attendance->total }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3"></td>
					<td>{{ $sumTotalPayrolls ?? NULL }}</td>
				</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>

	 

   </div>
</div>
</div>
</div>
</div>
@endsection@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Payroll </h1>
            <!-- end page-header -->
            <!-- begin panel -->
   <div class="row">
   <div class="col-md-5">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Profile</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
	           
	                     <div class="profile-view">
	                        <div class="profile-img-wrap">
	                           <div class="profile-img">
	                              <a href="#"><img src="{{ asset('img/admin.png') }}" alt=""></a>
	                           </div>
	                        </div>
			                        <div class="profile-basic">
			                           <div class="row">
			                              <div class="col-md-5">
			                                 <div class="profile-info-left">
			                                 	<br>
			                                    <h3 class="user-name m-t-0 mb-0">{{ $user->employee->name ?? null }}</h3>
			                                      
			                                    <h6 class="text-muted">{{ $user->employee->user->role->email ?? null }}</h6>
			                                    <div class="staff-id">Employee ID : {{ $user->employee->employee_no ?? null }}</div>
			                                    <div class="small doj text-muted">Date of Join : {{ $user->employee->joining_date ?? null }}</div>
			                                   
			                          
			                                 </div>
			                              </div>
			                              <div class="col-md-7">
			                              	<br>
			                                 <ul class="personal-info">
			                                    <li>
			                                       <div class="title">Phone:</div>
			                                       <div class="text"><a href="">{{ $user->employee->phone ?? null }}</a></div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Email:</div>
			                                       <div class="text"><a href=""><span class="_cf_email_" data-cfemail="c1abaea9afa5aea481a4b9a0acb1ada4efa2aeac">{{ $user->employee->user->email ?? NULL }}</span></a></div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Birthday:</div>
			                                       <div class="text">{{ $user->employee->birthday ?? null }}</div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Address:</div>
			                                       <div class="text">{{ $user->employee->address ?? null }}</div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Gender:</div>
			                                       <div class="text">{{ $user->employee->gender ?? null }}</div>
			                                    </li>
			                                    <li>
			                              </div>
			                              </li>
			                              </ul>
			                           </div>
			                        </div>
	                     </div>
	                     <div class="pro-edit"><a data-target="#profile_info" data-toggle="modal" class="edit-icon" href="#"><i class="fa fa-pencil"></i></a></div>
	                  </div>
	               </div>
	            </div>
	     
 <div class="col-md-7">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Attendance</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
			<table class="table table-bordered">
			<thead>
			<tr>
			<th>Date</th>
			<th>Time In</th>
			<th>Time Out</th>
			<th>Total Hours</th> 
			</tr>
			</thead>
			<tbody>
				@php 
				 	$sumTotalHours = 0;
				@endphp
				@foreach ($user->attendances as $attendance)
					@php
						$sumTotalHours  += $attendance['total'];
						$sumTotalPayrolls = $sumTotalHours + $user->employee->rates;
					@endphp
				<tr>
					<td>{{ $attendance->date }}</td>
					<td>{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</td>
					<td> {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}</td>
					<td>{{ $attendance->total }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3"></td>
					<td>{{ $sumTotalPayrolls ?? NULL }}</td>
				</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>

	 

   </div>
</div>
</div>
</div>
</div>
@endsection@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Payroll </h1>
            <!-- end page-header -->
            <!-- begin panel -->
   <div class="row">
   <div class="col-md-5">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Profile</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
	           
	                     <div class="profile-view">
	                        <div class="profile-img-wrap">
	                           <div class="profile-img">
	                              <a href="#"><img src="{{ asset('img/admin.png') }}" alt=""></a>
	                           </div>
	                        </div>
			                        <div class="profile-basic">
			                           <div class="row">
			                              <div class="col-md-5">
			                                 <div class="profile-info-left">
			                                 	<br>
			                                    <h3 class="user-name m-t-0 mb-0">{{ $user->employee->name ?? null }}</h3>
			                                      
			                                    <h6 class="text-muted">{{ $user->employee->user->role->email ?? null }}</h6>
			                                    <div class="staff-id">Employee ID : {{ $user->employee->employee_no ?? null }}</div>
			                                    <div class="small doj text-muted">Date of Join : {{ $user->employee->joining_date ?? null }}</div>
<<<<<<< HEAD
=======
			                                   
>>>>>>> 439c4c2429d012facdaa51a4e88198b32f7d1c97
			                                   
			                                 </div>
			                              </div>
			                              <div class="col-md-7">
			                              	<br>
			                                 <ul class="personal-info">
			                                    <li>
			                                       <div class="title">Phone:</div>
			                                       <div class="text"><a href="">{{ $user->employee->phone ?? null }}</a></div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Email:</div>
			                                       <div class="text"><a href=""><span class="_cf_email_" data-cfemail="c1abaea9afa5aea481a4b9a0acb1ada4efa2aeac">{{ $user->employee->user->email ?? NULL }}</span></a></div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Birthday:</div>
			                                       <div class="text">{{ $user->employee->birthday ?? null }}</div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Address:</div>
			                                       <div class="text">{{ $user->employee->address ?? null }}</div>
			                                    </li>
			                                    <li>
			                                       <div class="title">Gender:</div>
			                                       <div class="text">{{ $user->employee->gender ?? null }}</div>
			                                    </li>
			                                    <li>
			                              </div>
			                              </li>
			                              </ul>
			                           </div>
			                        </div>
	                     </div>
	                     <div class="pro-edit"><a data-target="#profile_info" data-toggle="modal" class="edit-icon" href="#"><i class="fa fa-pencil"></i></a></div>
	                  </div>
	               </div>
	            </div>
	     
 <div class="col-md-7">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Attendance</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
			<table class="table table-bordered">
			<thead>
			<tr>
			<th>Date</th>
			<th>Time In</th>
			<th>Time Out</th>
			<th>Total Hours</th> 
			</tr>
			</thead>
			<tbody>
				@php 
				 	$sumTotalHours = 0;
				@endphp
				@foreach ($user->attendances as $attendance)
					@php
						$sumTotalHours  += $attendance['total'];
						$sumTotalPayrolls = $sumTotalHours + $user->employee->rates;
					@endphp
				<tr>
					<td>{{ $attendance->date }}</td>
					<td>{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</td>
					<td> {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}</td>
					<td>{{ $attendance->total }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3"></td>
					<td>{{ $sumTotalPayrolls ?? NULL }}</td>
				</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>

	 

   </div>
</div>
</div>
</div>
</div>
@endsection