@extends('layouts.master')
@section('content')



<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Payroll </h1>
            <!-- end page-header -->
            <!-- begin panel -->
   <div class="row">
   <div class="col-md-12">
   <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title"> Payroll</h4>
         <div class="panel-heading-btn">
         </div>
      </div>
      <div class="panel-body">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Employee Name</th>
				<th>Employee No</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
			<tr>
				<td>{{ $user->id }}</td>
				<td>{{ $user->employee->name ?? null }}</td> 
				<td>{{ $user->employee->employee_no ?? null }}</td> 
				<td><a href="/payrolls/employees/{{ $user->id }}/manage" class="btn btn-primary btn-xs">Manage</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>
</div>
</div>
</div>
</div>
</div>






@endsection