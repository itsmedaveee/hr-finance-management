@extends('layouts.master')
@inject('stats','App\Stats') 
@section('content')

  
<div id="content" class="content">

	<h1>Dashboard</h1>

<div class="row">

<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-user fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Users</div>
<div class="stats-number">{{ $stats->totalUsers() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>

<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Employees</div>
<div class="stats-number">{{ $stats->totalEmployees() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>

<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-book fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Subjects</div>
<div class="stats-number">{{ $stats->totalSubjects() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>

<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-list fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Courses</div>
<div class="stats-number">{{ $stats->totalCourses() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-graduation-cap fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Students</div>
<div class="stats-number">{{ $stats->totalStudents() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-clock fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Schedules</div>
<div class="stats-number">{{ $stats->totalSchedules() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>
<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-building fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Departments</div>
<div class="stats-number">{{ $stats->totalDepartments() }}</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>

@endsection
