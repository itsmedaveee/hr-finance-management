
<div class="sidebar" id="sidebar">
<div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
<li class="menu-title">
<span>Main</span>
</li>

 
<li class=" {{ Request::is('attendances') ? 'active' : '' }}">
<a href="/attendances"><i class="la la-rocket"></i> <span> Attendances</span> </a>
</li>
 
<li  class=" {{ Request::is('leaves') ? 'active' : '' }}">
<a href="/leaves"><i class="la la-users"></i> <span>Leaves</span></a>
</li>

<li  class=" {{ Request::is('pending-leaves') ? 'active' : '' }}">
<a href="/pending-leaves"><i class="la la-file"></i> <span>Pending Leaves</span></a>
</li>
<li class="{{ Request::is('settings') ? 'active' : '' }}">
<a href="/settings"><i class="la la-cog"></i> <span>Settings</span></a>
</li>
<li class="submenu {{ Request::is('payrolls') ? 'active' : '' }}">
<a href="#"><i class="la la-money"></i> <span> Payroll </span> <span class="menu-arrow"></span></a>
<ul style="display: none;">
<li><a href="/payrolls" class="{{ Request::is('payrolls') ? 'active' : '' }}"> Employee Salary </a></li>
<li><a href="salary-view.html"> Payslip </a></li>
<li><a href="payroll-items.html"> Payroll Items </a></li>
</ul>
</li>
<li>

</div>
</div>
</div>
