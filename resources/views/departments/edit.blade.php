@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/departments">Departments</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Department </h1>
            <!-- end page-header -->
            <!-- begin panel -->

       <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Edit Department</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
	<form method="POST" action="/departments/{{ $department->id }}">
		@csrf
		{{ method_field('PATCH') }}
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<label class="col-form-label">Department Name <span class="text-danger">*</span></label>
					<input class="form-control" type="text" name="name" value="{{ $department->name }}">
				</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			</div>

		</div>
	</form>
</div>
</div>
</div>












@endsection