@extends('layouts.master')
@section('content')
<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/departments">Departments</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Department </h1>
            <!-- end page-header -->
            <!-- begin panel -->

       <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Add Department</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <form method="POST" action="/departments">
                     @csrf
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <label class="col-form-label">Department Name <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="name">
                              @if ($errors->has('name'))
                                 <span class="help-block">
                                     <strong style="color:red;">{{ $errors->first('name') }}</strong>
                                 </span>
                              @endif
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
          <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Department lists</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Department Name</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($departments as $department)
                        <tr>
                           <td>{{ $department->id }}</td>
                           <td>{{ $department->name }}</td>
                           <td>
                              <a href="/departments/{{ $department->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
                              <form method="POST" action="/departments/{{ $department->id }}" style="display: inline-block;">
                                 @csrf
                                 {{ method_field('DELETE') }}
                                 <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                              </form>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>




@endsection