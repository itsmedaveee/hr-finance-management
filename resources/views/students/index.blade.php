@extends('layouts.master')
@section('content')

 
<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">

            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Students </h1>
            <!-- end page-header -->
            <!-- begin panel -->
          <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Add Student</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               		<form method="POST" action="/students">
               			@csrf

                    <div class="row">
                      <div class="col-md-3">

         			<div class="form-group{{ $errors->has('student_no') ? ' has-error' : '' }}"> 
         				<label>Student ID</label>
         				<input type="text" name="student_no" class="form-control">
                        @if ($errors->has('student_no'))
                          <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('student_no') }}</strong>
                          </span>
                        @endif
         			</div>  
                                            
                    </div>     
                     <div class="col-md-3">                   
                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}"> 
                      <label>Firstname</label>
                      <input type="text" name="firstname" class="form-control">
                      @if ($errors->has('firstname'))
                          <span class="help-block">
                              <strong style="color:red;">{{ $errors->first('firstname') }}</strong>
                          </span>
                      @endif
                    </div>               			
                    </div>    
                     <div class="col-md-3{{ $errors->has('middlename') ? ' has-error' : '' }}">                
               			<div class="form-group"> 
               				<label>Middlename</label>
               				<input type="text" name="middlename" class="form-control">
                            @if ($errors->has('middlename'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('middlename') }}</strong>
                              </span>
                            @endif
               			</div>               			
                    </div>    

                     <div class="col-md-3">                
               			<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}"> 
               				<label>Lastname</label>
               				<input type="text" name="lastname" class="form-control">
                             @if ($errors->has('lastname'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('lastname') }}</strong>
                              </span>
                            @endif
               			</div>               			
                    </div> 
                     <div class="col-md-3">                   
               			<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}"> 
               				<label>Address</label>
               				<input type="text" name="address" class="form-control">
                            @if ($errors->has('address'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('address') }}</strong>
                              </span>
                            @endif
               			</div>               			
                    </div>  
                     <div class="col-md-3">                  
               			<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}"> 
               				<label>Contact No</label>
               				<input type="text" name="contact_no" class="form-control">
                            @if ($errors->has('contact_no'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('contact_no') }}</strong>
                              </span>
                            @endif
               			</div>               			
                    </div>    
                     <div class="col-md-3">                
               			<div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}"> 
               				<label>Birthdate</label>
               				<input type="date" name="birthdate" class="form-control">
                             @if ($errors->has('birthdate'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('birthdate') }}</strong>
                              </span>
                            @endif
               			</div>
                    </div>

            <div class="col-md-3">
              <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
              <label>Year Level</label>
              <select class="form-control" name="year">
              	<option value="" selected="" disabled="">Select Year Level</option>
                <option value="1">1st year</option>
              	<option value="2">2nd year</option>
              	<option value="3">3rd year</option>
              	<option value="4">4th year</option>
              </select>
              @if ($errors->has('year'))
              <span class="help-block">
                  <strong style="color:red;">{{ $errors->first('year') }}</strong>
              </span>
            @endif

              </div>  
              </div>  

       <div class="col-md-3">
              <div class="form-group{{ $errors->has('semister') ? ' has-error' : '' }}">
              <label>Semister</label>
              <select class="form-control" name="semister">
                <option value="" selected="" disabled="">Select Semister</option>
                <option value="1">1st Semister</option>
                <option value="2">2nd Semister</option>
              </select>
                @if ($errors->has('semister'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('semister') }}</strong>
                  </span>
                @endif

              </div>  
              </div>  
                    
               <div class="col-md-3">
						<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
	                      <label for="course">Course:</label>
	                      <select class="form-control" id="course" name="course">
	                        <option value="" disabled="" selected="">Select Course</option>
	                        @foreach ($courses as $courseId => $course )
	                        <option value="{{ $courseId }}">{{ $course }}</option>
	                        <span class="help-block">
	                            @if ($errors->has('course'))
                                    <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('course') }}</strong>
                                    </span>
								@endif
	                        @endforeach
	                      </select>
	                    </div>
                      </div>
                    <div class="col-md-3">
                    <div class="form-group{{ $errors->has('school_year_from') ? ' has-error' : '' }}"> 
                      <label>School Year From</label>
                      <input type="number" name="school_year_from" class="form-control">
                    <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('school_year_from') }}</strong>
                  </span>
                    </div>   
                    </div>   

              <div class="col-md-3{{ $errors->has('school_year_to') ? ' has-error' : '' }}">
                <div class="form-group"> 
                  <label>School Year To</label>
                  <input type="number" name="school_year_to" class="form-control">
                   <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('school_year_to') }}</strong>
                  </span>
                </div>   
                </div>   


{{-- 
						<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
	                      <label for="subject">Subject:</label>
	                      <select class="multiple-select2-subject form-control multiple" multiple  name="subject[]">
	                        @foreach ($subjects as $subjectId => $subject )
	                        <option value="{{ $subjectId }}">{{ $subject }}</option>
	                        <span class="help-block">
	                            @if ($errors->has('subject'))
                                    <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('subject') }}</strong>
                                    </span>
								@endif
                        @endforeach
	                      </select>
	                    </div> --}}


              <div class="col-md-12">
  							<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
  								<label>Email</label>
  								<input type="text" class="form-control" name="email">
  								@if ($errors->has('email'))
  								    <span class="help-block">
  								        <strong style="color:red;">{{ $errors->first('email') }}</strong>
  								    </span>
  								@endif
  							</div>
                </div>
              </div>
         


                  <div class="form-group">
                  	<button type="submit" class="btn btn-primary">Submit</button>
                  </div>

               			
               		</form>

               </div>
           </div>
       </div>
   
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Students</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               	<table class="table table-bordered">
               	<thead>
               		<tr>
               			<th>Student ID</th>
               			<th>Fullname</th>
               			<th>Course</th>
               			<th>Year</th>
               			<th>Options</th>
               		</tr>
               	</thead>
               	<tbody>
               		@foreach ($students as $student)
               		<tr>
						<td>{{ $student->student_no }}</td>
						<td>{{ $student->firstname }} {{ $student->middlename }} {{ $student->lastname }}</td>
						<td>

					
                      {{ $student->latestAssessment->course->code ?? NULL }}
               
        
						</td>

						<td>
						@if ($student->latestAssessment)
						    @if ($student->latestAssessment->year == 1)
						        1st Year
						    @elseif ($student->latestAssessment->year == 2)
						        2nd Year
						    @elseif ($student->latestAssessment->year == 3)
						        3rd Year
						    @elseif ($student->latestAssessment->year == 4)
						        4th Year
						    @elseif ($student->latestAssessment->year == 5)
						        5th Year
						    @endif
						@else
						    Please assign a course/level!
						@endif
						</td>

						<td>
						  <a href="/students/{{ $student->id }}/manage" class="btn btn-primary btn-xs">Manage</a>
                          <form method="POST" action="/students/{{ $student->id}}" style="display:inline-block">
                            @csrf 
                            {{ method_field('DELETE') }}
                            <button type="text" class="btn btn-danger btn-xs">Delete</button>
                          </form>
						</td>
					</tr>
               		@endforeach
               	</tbody>
               </table>
               </div>
           </div>
       </div>
   </div>



   </div>
</div>









@endsection

@push('scripts')
    <script type="text/javascript">
     
        $(".multiple-select2-subject").select2({
            placeholder: "Select a subject"
        });
    </script>
@endpush
