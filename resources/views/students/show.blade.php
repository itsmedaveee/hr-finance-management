@extends('layouts.master')
@section('content')

 
<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/admin-users">Admin Users</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Student Details</h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Student Details</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               	<table class="table table-hover table-bordered m-0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>FullName</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                    		<td></td>
                        <td>
                          {{ $student->firstname }}
                          {{ $student->middlename }}
                          {{ $student->lastname }}
                        </td>
                    	</tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>


     
 <div class="col-md-6">
    <div class="panel panel-default" >
       <div class="panel-heading" style="background:">
         <h4 class="panel-title"> Add Year Level</h4>
          <div class="panel-heading-btn">
             
          </div>
       </div>
       <div class="panel-body">
        <div class="row">

           <div class="col-md-12">
          <form method="POST" action="/students/{{ $student->id }}/manage">
            @csrf
            <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                        <label for="course">Course:</label>
                        <select class="form-control" id="course" name="course">
                          <option value="" disabled="" selected="">Select Course</option>
                          @foreach ($courses as $courseId => $course )
                          <option value="{{ $courseId }}">{{ $course }}</option>
                          <span class="help-block">
                              @if ($errors->has('course'))
                                    <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('course') }}</strong>
                                    </span>
                @endif
                          @endforeach
                        </select>
                      </div>
                      </div>



            <div class="col-md-12">
              <div class="form-group">
              <label>Year Level</label>
              <select class="form-control" name="year">
                <option value="" selected="" disabled="">Select Year Level</option>
                <option value="1">1st year</option>
                <option value="2">2nd year</option>
                <option value="3">3rd year</option>
                <option value="4">4th year</option>
              </select>

              </div>  
              </div>  

       <div class="col-md-12">
              <div class="form-group">
              <label>Semister</label>
              <select class="form-control" name="semister">
                <option value="" selected="" disabled="">Select Semister</option>
                <option value="1">1st Semister</option>
                <option value="2">2nd Semister</option>
              </select>

              </div>  
              </div>  


                    <div class="col-md-6">
                    <div class="form-group"> 
                      <label>School Year From</label>
                      <input type="number" name="school_year_from" class="form-control">
                    </div>   
                    </div>   

              <div class="col-md-6">
                <div class="form-group"> 
                  <label>School Year To</label>
                  <input type="number" name="school_year_to" class="form-control">
                </div>   
                </div>  
                <div class="col-md-6">
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div> 
                </div> 


       </div>
     </div>
   </form>
   </div>
   </div>



 <div class="col-md-6">
    <div class="panel panel-default" >
       <div class="panel-heading" style="background:">
         <h4 class="panel-title"> Year Level</h4>
          <div class="panel-heading-btn">
             
          </div>
       </div>
       <div class="panel-body">
       		<table class="table table-bordered" id="example">
       			<thead>
       				<tr>
						<th>Course</th>
						<th>Year Level</th>
            <th>Semister</th>
						<th>Status</th>
						<th>Options</th>
       				</tr>
       			</thead>
       			<tbody>
       				<tr>
				@forelse ($student->assessments as $assessment)
				<tr>
					<td>{{ $assessment->course->code }}</td>
					<td>
						@if ($assessment->year == 1)
						1st Year
						@elseif ($assessment->year == 2)
						2nd Year
						@elseif ($assessment->year == 3)
						3rd Year
						@elseif ($assessment->year == 4)
						4th Year
						@elseif ($assessment->year == 5)
						5th Year
						@endif
					</td>
          <td>{{ $assessment->semister }}</td>
					 <td>
                            @if ($assessment->status == 'For Payment')
                              <span class="label label-warning"> {{ $assessment->status }}</span>
                            @elseif ($assessment->status == 'Paid')
                              <span class="label label-success">{{ $assessment->status }}</span>
                            @elseif ($assessment->status == 'Verified')
                              <span class="label label-info">{{ $assessment->status }}</span>
                            @elseif ($assessment->status == 'For Partial')
                              <span class="label label-info">{{ $assessment->status }}</span>
                            @endif
                         </td>
					 <td>
					 {{-- 	<a href="{{ route('students.levels.subjects',[ $student, $level]) }}" class="btn btn-inverse btn-xs">Subjects</a> --}}
					 	<a href="{{ route('assessments.index', $assessment) }}" class="btn btn-primary btn-xs">Manage Subjects</a>
					 </td>
				</tr>
				 @empty
				<tr>
					<td colspan="4" class="text-center">No available levels</td>
				</tr>
				@endforelse
       			</tbody>
       			
       		</table>
       	</div>
       </div>
   </div>
</div>


</div>


 
@endsection


@push('scripts')


    <script type="text/javascript">


        $(".multiple-select2-subject").select2({
            placeholder: "Select a subject"
        });
    </script>
@endpush
