@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/admin-users">Admin Users</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Subjects for : {{ $level->student->firstname }} {{ $level->student->middlename }} {{ $level->student->lastname }}</h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-4">
            <div class="panel panel-default" >
               <div class="panel-heading">
                <h4 class="panel-title">Amount</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">


                <form action="{{ route('students.levels.subjects.update', [$student, $level]) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <input type="hidden" name="level" class="form-control" value="{{ $level->year }}">
                    <div class="form-group">
                        <label>Amount</label>
                        
                        <input type="text" name="" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
               </div>
           </div>
       </div>

       <div class="col-md-8">
            <div class="panel panel-default" >
               <div class="panel-heading">
                <h4 class="panel-title"> 
                	@if ($level->year == 1)
                        1st Year
                    @elseif ($level->year == 2)
                        2nd Year
                    @elseif ($level->year == 3)
                        3rd Year
                    @elseif ($level->year == 4)
                        4th Year
                    @elseif ($level->year == 5)
                        5th Year
                    @endif</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               	 <table class="table table-bordered table-hover">
                        <thead>
                            {{-- <tr>
                                <th colspan="3">1st Term</th>    
                            </tr> --}}

                            <tr>
                                <th>Code</th>
                                <th>Subject Title</th>
                                <th>Year Level</th>
                                <th>Amount</th>
                                {{-- <th>Options</th> --}}
                            </tr>
                        </thead>
                          <tbody>
                            @forelse ($level->subjects as $subject)
                                <tr>
                                    <td>{{ $subject->subject->code }}</td>
                                    <td>{{ $subject->subject->name }}</td>
                                    <td>
                                        @if ($subject->subject->year == 1)
                                                    1st Year
                                        @elseif ($subject->subject->year == 2)
                                            2nd Year
                                        @elseif ($subject->subject->year == 3)
                                            3rd Year
                                        @elseif ($subject->subject->year == 4)
                                            4th Year
                                        @elseif ($subject->subject->year == 5)
                                            5th Year
                                        @endif
                                    </td>
                                    <td>{{ $subject->subject->amount }}</td>
                             
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center">No subjects available</td>
                                </tr>
                            @endforelse
                                <tr class="f-w-700">
                                    <td colspan="3"> </td>
                                    <td >
                                       Total 


                                         
                                       <strong class="text-red"> {{ $subject->subject->sum('amount') - $subject->amount }} </strong>
                                    </td>
                                </tr>
                        </tbody>
                    </table>

               </div>
           </div>

            <div class="panel panel-default" >
               <div class="panel-heading">
                <h4 class="panel-title"> 
                    Bill
                </h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
       </div>
   </div>
</div>




   </div>














@endsection