@extends('layouts.app')

@section('content')
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
    <div class="login-box">

        <!--This is dark logo icon-->


        <div class="white-box">
            
 <div class="login">
            <!-- begin login-brand -->
            <div class="login-brand bg-default text-white text-center" style="background: black; ">
                 LOGIN
            </div>
            <!-- end login-brand -->

            <!-- begin login-content -->
            <div class="login-content" >
                <h4 class="text-center m-t-0 m-b-20">Great to have you back!</h4>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                        <input class="form-control" id="username" type="username" name="username" value="{{ old('username') }}" autofocus required="" placeholder="Enter Username">
                        @if ($errors->has('username'))
                        <div class="help-block with-errors">{{ $errors->first('username') }}</div>
                        @endif

                        </div>
                        </div>
                    <div class="form-group">
                    <div class="col-xs-12">
                    <input class="form-control" id="password" type="password" name="password" required="" placeholder="Enter Password">
                    @if ($errors->has('password'))
                    <div class="help-block with-errors">{{ $errors->first('password') }}</div>
                    @endif
                    </div>
                    </div>


                    <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Login</button>
                    </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
