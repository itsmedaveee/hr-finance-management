{{-- \
<div class="sidebar" id="sidebar">
<div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
<li class="menu-title">
<span>Main</span>
</li>
<li class="{{ Request::is('home') ? 'active' : '' }} ">
<a href="/employee/home"><i class="la la-dashboard"></i> <span> Dashboard</span> </a>
</li>

<li class=" {{ Request::is('attendances') ? 'active' : '' }}">
<a href="/attendances"><i class="la la-rocket"></i> <span> Attendances</span> </a>
</li>


<li  class=" {{ Request::is('leaves') ? 'active' : '' }}">
<a href="/leaves"><i class="la la-users"></i> <span>Leaves</span></a>
</li>

<li class="{{ Request::is('settings') ? 'active' : '' }}">
<a href="/settings"><i class="la la-cog"></i> <span>Settings</span></a>
</li>

</ul>
</li>
<li>

</div>
</div>
</div>
 --}}

 <div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
         <li class="has-sub {{ Request::is('employee/home') ? 'active' : '' }}">
            <a href="/employee/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>

         <li class="has-sub  {{ Request::is('attendances') ? 'active' : '' }} {{ Request::is('attendance-report') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-clock"></i>
               </div>
               <span>Attendance</span>
            </a>
            <ul class="sub-menu">   
               <li class="{{ Request::is('attendances') ? 'active' : '' }}"><a href="/attendances">Attendace </a></li>
                <li class="{{ Request::is('attendance-report') ? 'active' : '' }}"><a href="/attendance-report">Attendace Report</a></li>
            </ul>
         </li>       



 

         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>