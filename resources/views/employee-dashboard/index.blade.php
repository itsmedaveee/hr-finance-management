@extends('layouts.master')
@section('content')


@inject('stats','App\Stats') 
@section('content')

  
<div id="content" class="content">

	<h1>Certificate of Employee</h1>
 
 @if ($employee->status == 1)
<div class="form-group">
    <a href="/generate-certificate-of-employee/{{ $employee->id }}" class="btn btn-primary">Generate COE</a>
</div> 
@endif


@endsection