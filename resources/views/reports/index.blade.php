@extends('layouts.master')
@section('content')
<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Reports</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">Reports</li>
               </ul>
            </div>
         </div>
        </div>
         <div class="card-body">
       @if(auth()->user()->isAdmin())
            <form>
                <div class="form-group">
                    <label class="required" for="user">Employee</label>
                    <select class="form-control" name="user" id="user">
                        <option hidden>Select an employee</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ request()->input('user') == $user->id ? 'selected' : '' }}>{{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        @endif
      @if (auth()->user()->isAdmin() || request()->input('user') || $timeEntries)

      	{{-- 	@php 
      			dd(request()->input('user') || $timeEntries);
      		@endphp --}}
            <div class="row">
                <div class="{{ $chart->options['column_class'] }}">
                    <h3>{!! $chart->options['chart_title'] !!}</h3>
                    {!! $chart->renderHtml() !!}
                </div>

                <div class="col-md-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        Day
                                    </th>
                                    <th>
                                        Total time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dateRange as $date)
                                    <tr>
                                        <td>
                                            {{ $date }}
                                        </td>
                                        <td>
                                            {{ gmdate("H:i:s", $timeEntries[$date] ?? 0) }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <p class="text-center">Select an employee to view his report</p>
        @endif
    </div>
</div>
@endsection
@push ('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

@parents
@if ($chart)
{!! $chart->renderJs() !!}
@endif

<script>
$(function () {
    $('#user').change(function () {
        $(this).parents('form').submit();
        console.log(user)
    });
});
</script>
@endpush