<div class="modal fade" id="editAttendanceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Daily Time Record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form method="POST" action="/edit-my-attendance">

          @csrf
          {{ method_field('PATCH') }}

            <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
              <input type="hidden" class="form-control" id="id" name="id">
            
            </div>


            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            <label for="date">Date</label>
            <input type="date" class="form-control" id="date" name="date" placeholder="Enter Date" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">

            @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>

  {{--  
              <div class="form-group">
              <input type="time" class="form-control" id="in" name="time_in">
            
            </div> --}}

            <div class="form-group">
              <label>Timeout</label>
              <input type="time" class="form-control" id="out" name="time_out">
            
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
