@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Generate Report Attendance </h1>
            <!-- end page-header -->
         
<div class="row">

	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Generate Report</h4>
				<div class="panel-heading-btn">
				
				</div>
			</div>
		
			<div class="panel-body ">
						<form class="form-horizontal" method="GET" action="/attendance-reports"> 
						<div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
						    <label for="from" class="col-md-4 control-label ">From </label>
						    <div class="col-md-12">
						        <input id="from" type="date" class="form-control " name="from" value="{{ request('from') }}">

						        @if ($errors->has('from'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('from') }}</strong>
						            </span>
						        @endif
						    </div>
						</div>						

						<div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
						    <label for="to" class="col-md-4 control-label ">To </label>
						    <div class="col-md-12">
						        <input id="to" type="date" class="form-control " name="to" value="{{ request('to') }}">
						        @if ($errors->has('to'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('to') }}</strong>
						            </span>
						        @endif
						    </div>
						</div>

						<div class="form-group">
						    <div class="col-md-6 col-md-offset-4">

						        <button type="submit" class="btn btn-outline-primary">
						            Generate
						        </button>

						        <a href="" class="btn btn-outline-danger">
						            Cancel
						        </a>
						    </div>
						</div>
						</form>
					</div>
				</div>
				</div>

		<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">-</h4>
				
			</div>

			<div class="panel-body ">
				<div class="form-group">
					 <a href="{{ route('attendance-report/export', ['from' => request('from'), 'to' => request('to') ]) }}" class="btn btn-outline-primary" style="display: inline-block;">Generate Excel Reports</a>
				</div>
				<table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Date</th>
                           <th>Time In</th>
                           <th>Time Out</th>
                          {{--  <th>Status</th> --}}
                         {{--   <th>Total</th> --}}
                        </tr>
                     </thead>


                     <tbody>
                   @php
                     function formatMinutes($minutes) {
                          $now = \Carbon\Carbon::now();
                          $then = $now->copy()->addMinutes($minutes);
                          $hours = $now->diffInHours($then);
                          $minutes = $now->diffInMinutes($then) - ($hours * 60);

                          return \sprintf('%d:%02d', $hours, $minutes);
                     }

                           $totalMinutes = 0;
                     @endphp

               	       @foreach ($attendances as $attendance)
                                    <tr>
                                        <td>
                                            <div>{{ $attendance->date }}</div>
                                        </td>
                                        <td>
                                        	{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}
                                        </td>
                                        <td>
                                        	@if ($attendance->time_out)
                                        		{{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}
                                        	@endif
                                        </td>
                                      {{--  <td>
                                       	
                                          @php
                                             $start = new \Carbon\Carbon($attendance['time_in']);
                                             $end   = new \Carbon\Carbon($attendance['time_out']);
                                             $minutesToday  = $start->diffInMinutes($end);
                                             $totalMinutes += $minutesToday;
                                          @endphp

                                         
                                               {{ formatMinutes($totalMinutes) }}
                                             

                                       </td> --}}
                                          
                                    </tr>
                                @endforeach
                        
                     </tbody>
                  </table>


			</div>
		</div>
	</div>
</div>












@endsection