@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Attendances </h1>
            <!-- end page-header -->
            <!-- begin panel -->
            <div class="row">
            <div class="col-md-4">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">Add Attendance</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <form method="POST" action="/attendances">
                     @csrf
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="col-form-label">Date<span class="text-danger">*</span></label>
                              <input class="form-control" type="date" name="date">
                           </div>                           
                           <div class="form-group">
                              <label class="col-form-label">Time In<span class="text-danger">*</span></label>
                              <input class="form-control" type="time" name="time_in">
                           </div>

                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>

         @include ('attendances.modal.manage')
       <div class="col-md-8">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">Attendances</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
            
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Date</th>
                           <th>Time In</th>
                           <th>Time Out</th>
                          {{--  <th>Status</th> --}}
                           <th>Total</th>
                           <th>Options</th>
                        </tr>
                     </thead>


                     <tbody>  

                        @php 
                            $sumTotalHours = 0;
                        @endphp
 
               	       @foreach ($attendances as $date => $rows)

                                    <tr>
                                        <td>
                                            <div>{{ $date }}</div> 
                                        </td>
                                        <td>
                                            @foreach ($rows as $attendance)
                                                <div style="margin-bottom: 5px;">{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</div>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($rows as $attendance)
                                                <div style="margin-bottom: 5px;">
                                                    @if($attendance['time_out'])
                                                    {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}
                                                    @endif
                                                </div>

                                            @endforeach
                                        @php
                                        $sumTotalHours  += $attendance['total'];
                                        @endphp
                                        </td> 
                                        <td> 
                                            {{ $attendance['total'] }}
                                        </td>
                                            <td>
                                                @foreach ($rows as $attendance)
                                                    <button type="button" class="btn btn-primary btn-sm m-b-3" data-toggle="modal" data-target="#editAttendanceModal" 
                                                    data-id="{{ $attendance['id'] }}"
                                                    data-time-in="{{ Carbon\Carbon::parse($attendance['time_in'])->format('H:i')  }}"
                                                    data-time-out="{{ Carbon\Carbon::parse($attendance['time_out'])->format('H:i') }}"
                                                    >
                                                        <i class="fa fa-edit"></i> Manage
                                                    </button> 
                                                    <br>
                                                    <br>
{{--                                                   <a href="/attendances/{{ $attendance['id'] }}/edit" class="btn-primary btn-sm">Manage</a> --}}
                                                @endforeach
                                            </td>

                                          
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3"></td>
                                      <td>{{  $sumTotalHours   }}</td>
                                </tr>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


 
@endsection


@push('scripts')

<script>

    $('#editAttendanceModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('id') // Extract info from data-* attributes
      var timeIn = button.data('time-in')
      var timeOut = button.data('time-out')

      console.log(timeIn)
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-body #id').val(id)
      modal.find('.modal-body #in').val(timeIn)
      modal.find('.modal-body #out').val(timeOut)
    })
</script>



@endpush