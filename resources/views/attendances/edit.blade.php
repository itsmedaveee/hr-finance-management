@extends('layouts.master')
@section('content')

<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Attendance</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">Attendance</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Add Attendance</h4>
               </div>
               <div class="card-body">
                  <form method="POST" action="/attendances/{{ $attendance->id }}">
                     @csrf
                     {{ method_field('PATCH') }}
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="col-form-label">Date<span class="text-danger">*</span></label>
                              <input class="form-control" type="date" name="date" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                           </div>                           
                           <div class="form-group">
                              <label class="col-form-label">Time In<span class="text-danger">*</span></label>
                              <input class="form-control" type="time" name="time_in" value="{{ $attendance->time_in }}">
                           </div>
                           <div class="form-group">
                              <label class="col-form-label">Time Out<span class="text-danger">*</span></label>
                              <input class="form-control" type="time" name="time_out" value="{{ $attendance->time_out }}">
                           </div>

                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



@endsection