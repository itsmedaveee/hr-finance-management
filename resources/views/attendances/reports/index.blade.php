<!DOCTYPE html>
<html>
<head>
	<title>Monthly Reports</title>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body> 
	<br>
	<p align="center"><strong>DAILY TIME RECORD</strong></p>
	
	<p align="center"><strong>Official hours for arrival and departure</strong></p>
 <div class="table table-responsive"></div>
<table class="table table-bordered" style="width:50%;">
	<thead>
         	<tr>
         		<th colspan="3"><p align="center"><strong>For the month of : </strong></p></th>
          	</tr>
            <tr>
               <th width="15">Date</th>
               <th width="15">Time In</th>
               <th width="15">Time Out</th> 
            </tr>
         </thead>
         <tbody>
		 
         	   @foreach ($attendances as $date => $rows)

                                    <tr>
                                        <td>
                                            <div>{{ $date }}</div> 
                                        </td>
                                        <td>
                                            @foreach ($rows as $attendance)
                                                <div style="margin-bottom: 5px;">{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</div>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($rows as $attendance)
                                                <div style="margin-bottom: 5px;">
                                                    @if($attendance['time_out'])
                                                    {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}
                                                    @endif
                                                </div>

                                            @endforeach
                                    </tr>
                                @endforeach 
                     </tbody> 

</table>  
</body>
</html>