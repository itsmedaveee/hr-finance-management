@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Schedules </h1>
            <!-- end page-header -->
            <!-- begin panel -->
            <div class="row">
            <div class="col-md-4">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">Add Schedule</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

                  <form method="POST" action="/schedules">
                     @csrf
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">
                              <label class="col-form-label">Label<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="label">
                           @if ($errors->has('label'))
                                 <span class="help-block">
                                     <strong style="color:red;">{{ $errors->first('label') }}</strong>
                                 </span>
                              @endif
                           </div>                           
                           <div class="form-group{{ $errors->has('time_in') ? ' has-error' : '' }}">
                              <label class="col-form-label">Time In<span class="text-danger">*</span></label>
                              <input class="form-control" type="time" name="time_in">
                              @if ($errors->has('time_in'))
                                 <span class="help-block">
                                     <strong style="color:red;">{{ $errors->first('time_in') }}</strong>
                                 </span>
                              @endif
                           </div>
                           <div class="form-group{{ $errors->has('time_in') ? ' has-error' : '' }}">
                              <label class="col-form-label">Time Out<span class="text-danger">*</span></label>
                              <input class="form-control" type="time" name="time_out">
                              @if ($errors->has('time_out'))
                                 <span class="help-block">
                                     <strong style="color:red;">{{ $errors->first('time_out') }}</strong>
                                 </span>
                              @endif
                           </div>

                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>

        <div class="col-md-8">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">Schedules</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Label</th>
                           <th>Time In</th>
                           <th>Time Out</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($schedules as $schedule)
                        <tr>
                           <td>{{ $schedule->id }}</td>
                           <td>{{ $schedule->label }}</td>
                           <td>{{ $schedule->time_in }}</td>
                           <td>{{ $schedule->time_out }}</td>
                           <!-- <td><a href="/schedules/{{ $schedule->id }}/edit" class="btn btn-primary btn-sm">Edit</a> -->

                            <td>
                            <form method="POST" action="/schedules/{{ $schedule->id}}">

                                @csrf 
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>

                            </form>
                           
                           
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>







@endsection