@extends('layouts.master')
@section('content')

<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Edit Leave</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Leaves</a></li>
                  <li class="breadcrumb-item active">Leaves</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Edit Leave</h4>
               </div>
               <div class="card-body">
                    <form method="POST" action="/leaves/update/{{ $leave->id }}">
                     @csrf
                     {{ method_field('PATCH') }}
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="col-form-label">Type of Leave <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="types_leave" value="{{ $leave->types_leave }}">
                           </div>                           
                           <div class="form-group">
                              <label class="col-form-label">Date <span class="text-danger">*</span></label>
                              <input class="form-control" type="date" name="date" value="{{ $leave->date }}">
                           </div>                            
                           <div class="form-group">
                              <label class="col-form-label">Duration <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="duration" value="{{ $leave->duration }}">
                           </div>                            
                           <div class="form-group">
                              <label class="col-form-label">No of Days <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="no_of_days" value="{{ $leave->no_of_days }}">
                           </div>                          
                            <div class="form-group">
                              <label class="col-form-label">Reason <span class="text-danger">*</span></label>
                              <textarea class="form-control" type="text" name="reason">{{ $leave->reason }}</textarea>
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Update</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



@endsection