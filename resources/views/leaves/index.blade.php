@extends('layouts.master')
@section('content')

<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Leaves</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Leaves</a></li>
                  <li class="breadcrumb-item active">Leaves</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Add Leaves</h4>
               </div>
               <div class="card-body">
                  <form method="POST" action="/leaves">
                     @csrf
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="col-form-label">Type of Leave <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="types_leave">
                           </div>                           
                           <div class="form-group">
                              <label class="col-form-label">Date <span class="text-danger">*</span></label>
                              <input class="form-control" type="date" name="date">
                           </div>                              
                           <div class="form-group">
                              <label class="col-form-label">No of Days <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="no_of_days">
                           </div>                            
                           <div class="form-group">
                              <label class="col-form-label">Duration <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="duration">
                           </div>                          
                            <div class="form-group">
                              <label class="col-form-label">Reason <span class="text-danger">*</span></label>
                              <textarea class="form-control" type="text" name="reason"></textarea>
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>

           <div class="col-sm-12" style="margin-top: 30px ">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Leaves</h4>
               </div>
               <div class="card-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Type of Leave</th>
                           <th>Duration</th>
                           <th>Date</th>
                           <th>Reason</th>
                           <th>Status</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($leaves as $leave)
                        <tr>
                           <td>{{ $leave->id }}</td>
                           <td>{{ $leave->types_leave }}</td>
                           <td>{{ $leave->duration }}</td>
                           <td>{{ $leave->date }}</td>
                           <td>{{ $leave->reason }}</td>
                           <td>{{ $leave->status }}</td>
                           <td>
                              <a href="/leaves/{{ $leave->id }}/edit" class="btn btn-primary btn-sm">Edit</a>
                              <form method="POST" action="/leaves/{{ $leave->id }}" style="display:inline-block;">
                                 @csrf 
                                 {{ method_field('DELETE') }}
                                 <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                              </form>

                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

 


@endsection