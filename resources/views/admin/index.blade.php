@extends('layouts.master')
@section('content')
 
<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/admin-users">Admin Users</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Admin Users </h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Add Admin User</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               		<form method="POST" action="/admin">
               			@csrf 
               			 <div class="col-sm-12">
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
               <label class="col-form-label">Username <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="username" >
               @if ($errors->has('username'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('username') }}</strong>
                  </span>
               @endif
            </div>
         </div>     
          <div class="col-sm-12">      
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
               <label class="col-form-label">Email <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="email" >
                @if ($errors->has('email'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('email') }}</strong>
                  </span>
               @endif
            </div>
         </div>
         <div class="col-sm-12">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
               <label class="col-form-label">Password</label>
               <input class="form-control" type="password" name="password">
               @if ($errors->has('password'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('password') }}</strong>
                  </span>
               @endif
            </div>
      

       			<div class="form-group">
       				<button type="submit" class="btn btn-primary ">Submit</button>
       				
       			</div>
               		</form>
               </div>
            </div>
         </div>
         <br>
           <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title"> Admin Users</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Username</th>
                           <th>Email</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($admins as $admin)
                        <tr>
                           <td>{{ $admin->id }}</td>
                           <td>{{ $admin->username }}</td>
                           <td>{{ $admin->email }}</td>
                           <td><a href="/admin-users/{{ $admin->id }}/edit" class="btn btn-primary btn-sm">Edit</a></td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>

   </div>
</div>


@endsection