@extends('layouts.master')
@section('content')

<div id="content" class="content">
<ol class="breadcrumb float-xl-right">
   <li class="breadcrumb-item"><a href="/home">Home</a></li>
   <li class="breadcrumb-item"><a href="/students">Student</a></li>
</ol>
<h1 class="page-header">Student Assess</h1>
<div class="row">
	<div class="col-md-12">
	   <div class="panel panel-default" >
	      <div class="panel-heading">
	         <h4 class="panel-title"> Student </h4>
	         <div class="panel-heading-btn">
	          
	         </div>
	      </div>
	      <div class="panel-body">
	      	<table class="table table-bordered">
	      		<thead>
	      			<th>FullName</th>
	      			<th>Created At</th>
	      		</thead>
	      		<tbody>
	      			<tr>
	      				<td>{{ $assessment->student->firstname }} {{ $assessment->student->middlename }} {{ $assessment->student->lastname }}

	      				</td>
	      				<td>
	      					{{ $assessment->created_at->toFormattedDateString() }}
	      				</td>
	      			</tr>
	      		</tbody>
	      		
	      	</table>

	      </div>
	  </div>
	</div>


	<div class="col-md-4">
	   <div class="panel panel-default" >
	      <div class="panel-heading">
	         <h4 class="panel-title"> Add Subject </h4>
	         <div class="panel-heading-btn">
	          
	         </div>
	      </div>
	      <div class="panel-body">
	     		 <form method="POST" action="/assessments/{{ $assessment->id }}">
	     		 	@csrf
	         		<div class="form-group">
	         			<label>Subject</label>

	         			<select class="multiple-select2-subject form-control" multiple name="subject[]">
	         				@foreach ($subjects as $subjectId => $subject)
	         				<option value="{{ $subjectId }}">{{ $subject }}</option>
	         				@endforeach
	         			</select>

	         		</div>

	         		<div class="form-group">
	         			<label>Date</label>
	         			<input type="date" name="date" class="form-control">
	         		</div>

	         		<div class="form-group">
	         			<button type="submit" class="btn btn-primary">Submit</button>
	         		</div>
	         </form>

	      </div>
	  </div>
	</div>
	<div class="col-md-8">
	   <div class="panel panel-default" >
	      <div class="panel-heading">
	         <h4 class="panel-title"> Subjects </h4>
	         <div class="panel-heading-btn">
	          
	         </div>
	      </div>
	      <div class="panel-body">
	    
	      	<table class="table table-bordered">
	      		<thead>
	      			<tr>
	      				<th>Code</th>
	      				<th>Title</th>
	      				<th>Unit</th>
	      				<th>Amount</th>
	      			</tr>
	      		</thead>
	      		<tbody>
	      			@foreach ($assessment->subjectFees as $subject)
	      			<tr>
	      				<td>{{ $subject->code }}</td>
	      				<td>{{ $subject->name }}</td>
	      				<td>{{ $subject->unit }}</td>
	      				<td>{{ $subject->amount }}</td>
	      				<td>
	      					<form method="POST" action="/subject-remove/{{ $subject->id }}">
	      						
	      						{{ method_field('DELETE') }}
	      						@csrf

	      						<button type="submit" class="btn btn-danger btn-xs">Remove</button>

	      					</form>
	      					</td>
	      			</tr>
	      			@endforeach
	      			<tfoot>
	      				<tr>
	      					<td colspan="3"></td>
	      					<td>Total: {{ $assessment->totalAmount() }}</td>
	      				</tr>
	      			</tfoot>
	      		</tbody>
	      	</table>

	      	<form method="POST" action="/assesments-status/{{ $assessment->id }}">
            @csrf 
            {{ method_field('PATCH') }}
            	<div class="form-group">
   				<button class="btn btn-block btn-primary">Proceed to the Cashier</button>
   				</div>
            </div>	
         </form>

	      </div>

	  </div>
	</div>
</div>


 

@endsection

@push('scripts')
    <script type="text/javascript">
     
        $(".multiple-select2-subject").select2({
            placeholder: "Select a subject"
        });
    </script>
@endpush
