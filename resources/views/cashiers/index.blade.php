@extends('layouts.master')
@section('content')
 <div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/cashiers">Cashier</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Cashier</h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Cashier</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               		<form method="POST" action="/cashiers">
               			@csrf 
               			 <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Username <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="username" >
            </div>
         </div>     
          <div class="col-sm-12">      
          <div class="form-group">
               <label class="col-form-label">Email <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="email" >
            </div>
         </div>
         <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Password</label>
               <input class="form-control" type="password" name="password">
            </div>
      

       			<div class="form-group">
       				<button type="submit" class="btn btn-primary ">Submit</button>
       				
       			</div>
               		</form>
               </div>
            </div>
         </div>
         <br>
           <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title"> Cashier Users</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Username</th>
                           <th>Email</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($cashiers as $cashier)
                        <tr>
                           <td>{{ $cashier->id }}</td>
                           <td>{{ $cashier->username }}</td>
                           <td>{{ $cashier->email }}</td>
                           <td><a href="" class="btn btn-primary btn-sm">Edit</a></td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>

   </div>
</div>


@endsection