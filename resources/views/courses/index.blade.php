@extends('layouts.master')
@section('content')
<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="/home">Home</a></li>
					<li class="breadcrumb-item"><a href="/courses">Courses</a></li>
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Courses </h1>
				<!-- end page-header -->
				<!-- begin panel -->
				<div class="row">
				<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Add Course</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">
						<form  method="POST" action="/courses">
							@csrf


				
							<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
								<label>Code</label>
								<input type="text" class="form-control" name="code">
								@if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('code') }}</strong>
                                    </span>
								@endif
							</div>
							<div class="form-group">
								<label>Title</label>
								<input type="text" class="form-control" name="title">
								@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong style="color:red;">{{ $errors->first('title') }}</strong>
                                    </span>
								@endif
							</div>
					
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>

						</form>
					</div>
				</div>
				<!-- end panel -->
			</div> 

					<div class="col-md-8">
			<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Courses list</h4>
						<div class="panel-heading-btn">
							
						</div>
					</div>
					<div class="panel-body">

						<table class="table table-bordered table-hover" id="admin-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Code</th>
									<th>Title</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($courses as $course)
								<tr>
									<td>{{ $course->id }}</td>
									<td>{{ $course->code }}</td>
									<td>{{ $course->title }}</td>
									<td>
										<a href="/courses/{{ $course->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
										<form method="POST" action="/courses/{{ $course->id }}" style="display:inline-block;">
											@csrf
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger btn-xs">Delete</button>
											
										</form>

									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</span>




@endsection