@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/subjects">Subjects</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Subjects </h1>
            <!-- end page-header -->
            <!-- begin panel -->

       <div class="row">
            <div class="col-md-4">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Add Subject</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               		<form method="POST" action="/subjects">
               			@csrf 

               	


               			<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
               				<label>Code</label>
               				<input type="text" name="code" class="form-control">
                           @if ($errors->has('code'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('code') }}</strong>
                              </span>
                            @endif
               			</div>               			

               			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
               				<label>Name</label>
               				<input type="text" name="name" class="form-control">
                             @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('name') }}</strong>
                              </span>
                            @endif
               			</div>  

               			<div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
               				<label>Unit</label>
               				<input type="text" name="unit" class="form-control">
                           @if ($errors->has('unit'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('unit') }}</strong>
                              </span>
                            @endif
               			</div>  


						<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
               				<label>Year Level</label>
               				<select class="form-control" name="year" >
               					<option selected="" value="" disabled="">Select Year</option>
                              <option value="1">1st year</option>
               					<option value="2">2nd year</option>
               					<option value="3">3rd year</option>
               					<option value="4">4th year</option>
               				</select>
                            @if ($errors->has('year'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('year') }}</strong>
                              </span>
                            @endif
               				
               			</div>  
               			             			
               			<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
               				<label>Amount</label>
               				<input type="text" name="amount" class="form-control">
                             @if ($errors->has('amount'))
                              <span class="help-block">
                                  <strong style="color:red;">{{ $errors->first('amount') }}</strong>
                              </span>
                            @endif
               			</div>

               			<div class="form-group">
               				<button type="submit" class="btn btn-primary">Submit</button>
               			</div>
               		</form>

               </div>
           </div>
           </div>
            <div class="col-md-8">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Subjects</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               	<table class="table table-bordered">
               		<thead>
               			<th>Code</th>
               			<th>Name</th>
               			<th>Amount</th>
               			<th>Unit</th>
               			<th>Actions</th>
               		</thead>
               		<tbody>
               			@foreach ($subjects as $subject)
               			<tr>
               				<td>{{ $subject->code }}</td>
               				<td>{{ $subject->name }}</td>
               				<td>{{ $subject->amount }}</td>
               				<td>{{ $subject->unit }}</td>
               				<td><a href="/subjects/{{ $subject->id }}/edit" class="btn btn-primary btn-xs">Edit</a></td>
               			</tr>
               		</tbody>
				@endforeach
               		{{-- @php 
               			$subjectTotal = $subject->amount += $subject->amount;

               			dd($subjectTotal);
               		@endphp	 --}}
               		{{-- <tfoot>
               			<tr>
               				<td colspan="2"></td>
               				<td> Total:<span class="text-red"><b> {{ $subject->sum('amount') }} </b></span></td>
               			</tr>
               		</tfoot> --}}
               	
               	</table>
   </div>
</div>
</div>
</div>
</div>






@endsection