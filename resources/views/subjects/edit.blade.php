@extends('layouts.master')
@section('content')





<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/subjects">Subjects</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Subjects </h1>
            <!-- end page-header -->
            <!-- begin panel -->

       <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Edit Subject</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               		<form method="POST" action="/subjects/{{ $subject->id }}">
               			@csrf 
               			{{ method_field ('PATCH') }}

			

               			<div class="form-group">
               				<label>Code</label>
               				<input type="text" name="code" class="form-control" value="{{ $subject->code }}">
               			</div>               			

               			<div class="form-group">
               				<label>Name</label>
               				<input type="text" name="name" class="form-control" value="{{ $subject->name }}">
               			</div>                			

               			<div class="form-group">
               				<label>Unit</label>
               				<input type="text" name="unit" class="form-control" value="{{ $subject->unit }}">
               			</div>   

						<div class="form-group">
               				<label>Year Level</label>
               				<select class="form-control" name="year">
               					<option value="1">1st year</option>
               					<option value="2">2nd year</option>
               					<option value="3">3rd year</option>
               					<option value="4">4th year</option>
               				</select>
               				
               			</div>  
               			        
               			        
               			<div class="form-group">
               				<label>Amount</label>
               				<input type="text" name="amount" class="form-control" value="{{ $subject->amount }}">
               			</div>

               			<div class="form-group">
               				<button type="submit" class="btn btn-primary">Update</button>
               			</div>
               		</form>

               </div>
           </div>
           </div>



@endsection