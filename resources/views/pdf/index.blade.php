<!DOCTYPE html>
<html>
   <head>
      <title>COR</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body>
 <div class="col-md-12"> 
        <table class="table-bordered table-sm " width="100%" align="center">
          <tbody>
            <tr> 
              <td style="background: #00acac; color: #000">
                  <p class="text-center">
                    <strong>
                      SANTA RITA COLLEGE OF PAMPANGA <br> 
                      San Jose, Santa Rita Pampanga <br>
                      Tel No: (045) 900-5007<br>
                    </strong>
                  </p>
              </td>
            </tr>
          </tbody>
        </table>

        <table class="table table-bordered table-sm">
        	<thead>
            <tr>
              <td colspan="4" style="background: #00acac; color: #000; text-align: center;">Certificate of Registration</td>
            </tr>
        		<tr>
        			<td>Student ID: {{ $assessment->student->student_no }}</td>
        			<td></td>
              <td></td>
        			<td>Date: {{ $assessment->date }}</td>
        		</tr>
        	</thead>
        	<tbody>
        		<tr>
        			<td>Name : {{ $assessment->student->firstname }} {{ $assessment->student->middlename }} {{ $assessment->student->lastname }}</td>
              <td></td>
              <td colspan="5">Course: {{ $assessment->course->title }}</td>.
             
        		</tr>
            <tr>
              <td colspan="5">Address: {{ $assessment->student->address }}</td>

            </tr>
            <tr>
              <td>Year Level: 
                @if ($assessment->year == 1)
                   {{ $assessment->year }}st Year
                @elseif ($assessment->year == 2)
                   {{ $assessment->year }}nd Year     
                @elseif ($assessment->year == 3)
                   {{ $assessment->year }}rd Year
                @elseif ($assessment->year == 4)
                   {{ $assessment->year }}th Year
                @endif
              </td>
              <td>Semister : 1 </td>
              <td>School Year : {{ $assessment->school_year_from }} - {{ $assessment->school_year_to }}</td>
              <td>Status : {{ $assessment->status }} </td>
            </tr>
          </tbody>
        </table>
        <br>
        <br>
        <table class="table table-bordered table-sm">
          <tbody>
            <tr>

              <td style="background: #00acac; color: #000">Code</td>
              <td  style="background: #00acac; color: #000">Subject Title</td>
              <td colspan="4"  style="background: #00acac; color: #000">Unit</td>
              
            </tr>
          @foreach ($assessment->subjectFees as $subject)
            <tr>
              <td >{{ $subject->code }}</td>
              <td>{{ $subject->name }}</td>
              <td colspan="4">{{ $subject->unit }}</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="4">Total No of Subjects : <strong> {{ $assessment->subjectFees->count() }}</strong></td>
          </tr>
          <tr>
            <td colspan="4">Total No of Units : <strong> {{ $assessment->totalUnit() }} </strong> </td>
          </tr>

        	</tbody>
        </table>
      </div>



 



</body>
</html>
