<!DOCTYPE html>
<html>
   <head>
      <title>Report</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body> 

   	<div class="col-md-12"> 
        <table class="table " width="100%" align="center">
          <tbody>
            <tr> 
               <td style="background: #00acac; color: #000">
                  <p class="text-center">
                    <strong>
                      SANTA RITA COLLEGE OF PAMPANGA <br> 
                      San Jose, Santa Rita Pampanga <br>
                      Tel No: (045) 900-5007<br>
                    </strong>
                  </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <br>
      <table class="table table-bordered">
      	<thead>
      		<tr>
      			<th>Name</th>
      			<th>Employee ID</th>
      		</tr>
      	</thead>
      	<tbody>
      		<tr>
      			<td>{{ $user->employee->name ?? null }}</td>
      			<td>{{ $user->employee->employee_no ?? null }}</td>
      		</tr>
      	</tbody>
      </table>
      <table class="table table-bordered">
      	<tr>
      		<th>Date</th>
			<th>Time In</th>
			<th>Time Out</th>
			<th>Total Hours</th> 
      </tr>
			</thead>
			<tbody>
				@php 
				 	$sumTotalHours = 0;
				@endphp
				@foreach ($user->attendances as $attendance)
					@php
						$sumTotalHours  += $attendance['total'];
						$sumTotalPayrolls = $sumTotalHours * $user->employee->rates;
					@endphp
				<tr>
					<td>{{ $attendance->date }}</td>
					<td>{{ Carbon\Carbon::parse($attendance['time_in'])->format('h:i A') }}</td>
					<td> {{ Carbon\Carbon::parse($attendance['time_out'])->format('h:i A') }}</td>
					<td>{{ $attendance->total }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3">Grand Total</td>
					<td>{{ $sumTotalPayrolls ?? NULL }}</td>
				</tr>
			</tbody>
			</table>
</body>
</html>