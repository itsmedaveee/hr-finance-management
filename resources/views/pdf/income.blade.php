<!DOCTYPE html>
<html>
   <head>
      <title>Income</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body>
 <div class="col-md-12"> 
        <table class="table " width="100%" align="center">
          <tbody>
            <tr> 
               <td style="background: #00acac; color: #000">
                  <p class="text-center">
                    <strong>
                      SANTA RITA COLLEGE OF PAMPANGA <br> 
                      San Jose, Santa Rita Pampanga <br>
                      Tel No: (045) 900-5007<br>
                    </strong>
                  </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <br>

     <div class="col-md-12"> 
      <div class="form-group">
       <table class="table table-bordered table-sm">
            <thead>  
              <tr>
                <th colspan="2" style="text-align: center; background: #00acac; color: #000;">Monthly Report</th>
              </tr>
              <th>Date</th>
              <th>Amount</th>
            </thead>
            @if (request()->has('month'))
            <tbody>
              @foreach ($assessments as $assessment)
              <tr>
                <td >{{ $assessment->created_at->toFormattedDateString() }}</td>
                <td >{{ $assessment->amount }}</td>
              </tr>

              @if ($loop->last)
              <tr>
                <td colspan="1"></td>
                <td>Total : {{ $assessment->sum('amount') }}</td>
              </tr>
              @endif
            
              @endforeach
                
            </tbody>
            @endif
          </table>
      </div>
    </body>
    </html>
