<!DOCTYPE html>
<html>
   <head>
      <title>CERTIFICATE</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style type="text/css">
      </style>
   </head>
   <body>
 <div class="col-md-12"> 
        <table class="table " width="100%" align="center">
          <tbody>
            <tr> 
               <td style="background: #00acac; color: #000">
                  <p class="text-center">
                    <strong>
                      SANTA RITA COLLEGE OF PAMPANGA <br> 
                      San Jose, Santa Rita Pampanga <br>
                      Tel No: (045) 900-5007<br>
                    </strong>
                  </p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br>
      <br>
      <hr>
      <table align="center">
          <thead>
              <tr>
                  <th><h1  style="color:blue; font-family: Anton;" align="center"><strong>CERTIFICATE OF EMPLOYMENT</strong></h1></th>
              </tr>
              <tr>
                  <th><h6 align="center"><i>This is to certify that</i></h6></th>
              </tr>
              <tr>
                  <th><h3 align="center"><u><strong>{{ $employee->name }}</strong></u></h3></th>
              </tr>
              <tr>
                  <th><h6 align="center"><i>has been employed in</i></h6></th>
              </tr>  
              <tr>
                  <th><h4 align="center">Santa Rita College</h4></th>
              </tr>
              <tr>
                <th><h4 align="center">as</h4></th>
              </tr>
              <tr>
                <th><h4 align="center"><strong>Registrar</strong></h4></th>
              </tr>
              <tr>
                <th> <h5  align="center">Work Period : {{ $employee->joining_date->toFormattedDateString() }} To : {{ $employee->updated_at->toFormattedDateString() }}</h5>  </th>  
              </tr>
              <tr>
                <th></th>
              </tr>       
              <tr>
                <td><p><i>This is certification is being issued upon the request of the aforementioned name for whatever  lawful  </p><p align="center">purpose it may serve him best</i></p></td>
              </tr>
              <tr>
                <td></td>
              </tr>

              <tr>
                <td> <p align="center">Given this <b> {{ $employee->updated_at->toFormattedDateString() }}</b> at Santa Rita Pampanga College</p></td>
              </tr>
          </thead>
      </table>
  </body>
  </html>