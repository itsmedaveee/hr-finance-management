@extends('layouts.master')
@section('content')
 
<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Admin</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">Admin</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Admin</h4>
               </div>
               <div class="card-body">

               		<form method="POST" action="/finance">
               			@csrf 
               			 <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Username <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="username" >
            </div>
         </div>     
          <div class="col-sm-12">      
          <div class="form-group">
               <label class="col-form-label">Email <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="email" >
            </div>
         </div>
         <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Password</label>
               <input class="form-control" type="password" name="password">
            </div>
      

       			<div class="form-group">
       				<button type="submit" class="btn btn-primary btn-xs">Update</button>
       				
       			</div>
               		</form>
               </div>
            </div>
         </div>
         <br>
           <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Admin list</h4>
               </div>
               <div class="card-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Username</th>
                           <th>Email</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($finances as $finance)
                        <tr>
                           <td>{{ $finance->id }}</td>
                           <td>{{ $finance->username }}</td>
                           <td>{{ $finance->email }}</td>
                           <td><a href="" class="btn btn-primary btn-sm">Edit</a></td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>

   </div>
</div>


@endsection