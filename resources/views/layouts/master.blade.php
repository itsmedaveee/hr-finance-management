 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SRC</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/red.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

    
 
  <style type="text/css">
      
      .h-30px {

        height: 30px!important;
      }
      .rounded {
        border-radius: 4px!important;
      }
  </style>
 
</head>
  
    <body>
 <!-- begin #page-loader -->
    <div id="page-loader" class="fade show">
        <span class="spinner"></span>
    </div>
    <!-- end #page-loader -->
    <!-- begin #page-container -->
        <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

@include ('layouts.header')
@if(auth()->user()->isAdmin())
	@include ('layouts.sidebar')
@elseif(auth()->user()->isRegistrar())
	@include('employee-dashboard.sidebar.index')
@elseif(auth()->user()->isFinance())
	@include('finance-dashboard.sidebar.index')
@elseif(auth()->user()->isCashier())
  @include('cashier-dashboard.sidebar.index')


@elseif(auth()->user()->isStudent())
  @include('students-dashboard.sidebar.index')
@endif

@yield ('content')


   <script src="{{ asset('/js/app.min.js') }}"></script>

    <script src="{{ asset('/js/apple.min.js') }}"></script>


    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/js/toastr.min.js') }}"></script>
    <script src="{{ asset('/js/select2.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
 



 

    <script>
       
   
        @if(Session::has('success'))
            toastr.success("{{ Session::get('success') }}");
            {{ session()->forget('success') }}
        @endif

        @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
        @endif

        @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
        @endif

        @if(Session::has('error'))
            toastr.error("{{ Session::get('error') }}");
        @endif

        
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    </script>
    
<script>
     $(document).ready(function() {
        $('#slimScroll').slimScroll({
            height: '250px'
        });
    });
</script>

    
    @stack('scripts')

{{--     <script src="{{ asset('assets/plugins/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatable/DataTables-1.10.18/js/dataTables.bootstrap.min.js') }}"></script> --}}
</body>
</html>