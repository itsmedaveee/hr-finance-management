<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->name }}
                  <small></small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
         <li class="has-sub {{ Request::is('home') ? 'active' : '' }}">
            <a href="/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>



         <li class="has-sub {{ Request::is('admin') ? 'active' : '' }} {{ Request::is('employees') ? 'active' : '' }}  {{ Request::is('students') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-user-md"></i>
               </div>
               <span>Manage Users</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="/admin">Admin Users</a></li>
               <li class="{{ Request::is('employees') ? 'active' : '' }}"><a href="/employees">Employee Users</a></li>
            </ul>
         </li>

         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>