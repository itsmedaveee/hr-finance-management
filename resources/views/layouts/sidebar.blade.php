<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
         <li class="has-sub {{ Request::is('home') ? 'active' : '' }}">
            <a href="/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>



         <li class="has-sub {{ Request::is('admin-users') ? 'active' : '' }} {{ Request::is('employees') ? 'active' : '' }}   {{ Request::is('cashiers') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-user-md"></i>
               </div>
               <span>Manage Users</span>
            </a>
            <ul class="sub-menu">   
             <li class="{{ Request::is('admin-users') ? 'active' : '' }}"><a  href="/admin-users">Admin users</a></li>

               <li class="{{ Request::is('employees') ? 'active' : '' }}"><a href="/employees">Employee Users</a></li>
            </ul>
         </li>       


         <li class="has-sub {{ Request::is('schedules') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-file"></i>
               </div>
               <span>Schedules</span>
            </a>
            <ul class="sub-menu">   
             <li><a class="{{ Request::is('schedules') ? 'active' : '' }}" href="/schedules">Schedule</a></li>
            </ul>
         </li>        

         <li class="has-sub {{ Request::is('departments') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
              <i class="fa fa-list"></i> 
               </div>
               <span>Departments</span>
            </a>
            <ul class="sub-menu">   
            <li class="{{ Request::is('departments') ? 'active' : '' }}"><a href="/departments">Department</a></li>
            </ul>
         </li>

{{-- 
            <li class="has-sub {{ Request::is('courses') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
                  <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" />
              <i class="fa fa-tasks"></i> 
               </div>
               <span>Courses</span>
            </a>
            <ul class="sub-menu">   
            <li class="{{ Request::is('courses') ? 'active' : '' }}"><a href="/courses">Courses</a></li>
            </ul>
         </li>
 --}}

{{-- 
         <li class="has-sub {{ Request::is('subjects') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img"> 
              <i class="fa fa-book"></i> 
               </div>
               <span>Subjects</span>
            </a>
            <ul class="sub-menu">   
            <li class="{{ Request::is('subjects') ? 'active' : '' }}"><a href="/subjects">Subjects</a></li>
            </ul>
         </li>
 --}}
             {{-- <li class="has-sub  {{ Request::is('students') ? 'active' : '' }}  {{ Request::is('cashiers') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
                  <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" />
               <i class="fa fa-users"></i>
               </div>
               <span>Students</span>
            </a>
            <ul class="sub-menu">   
               <li class="{{ Request::is('students') ? 'active' : '' }}"><a href="/students">Student </a></li>
            </ul>
         </li>        --}}

             <li class="has-sub  {{ Request::is('attendances') ? 'active' : '' }} {{ Request::is('attendance-report') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-clock"></i>
               </div>
               <span>Attendance</span>
            </a>
            <ul class="sub-menu">   
               <li class="{{ Request::is('attendances') ? 'active' : '' }}"><a href="/attendances">Attendace </a></li>
               <li class="{{ Request::is('attendance-report') ? 'active' : '' }}"><a href="/attendance-report">Attendace Report</a></li>
            </ul>
         </li>       
       <li class="has-sub  {{ Request::is('payrolls') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-money"></i>
               </div>
               <span>Payroll</span>
            </a>
            <ul class="sub-menu">   
               <li class="{{ Request::is('payrolls') ? 'active' : '' }}"><a href="/payrolls">Payrolls </a></li>
            </ul>
         </li>       


         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>