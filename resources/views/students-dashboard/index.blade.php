@extends('layouts.master')
@section('content')



<div id="content" class="content content-full-width">

<div class="profile">
<div class="profile-header">

<div class="profile-header-cover"></div>

<div class="profile-header-content" id="particles-js">

   <div class="profile-header-img" >
      <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
   </div>

<div class="profile-header-info" >
<h4 class="mt-0 mb-1">{{ $user->student->firstname ?? null }} {{ $user->student->middlename ?? null }} {{ $user->student->lastname ?? null }}</h4>
<p class="mt-0 mb-1"></p>

     <a href="/settings" class="btn btn-primary btn-xs">Edit Profile</a>

</p>

</div>
</div>


<ul class="profile-header-tab nav nav-tabs" style="background: #f0f8ff21">
<li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;"></a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
</ul>
</div>
</div>
<div class="profile-content">
   <div class="form-group">
  

{{--    <form method="POST" action="/removed-request-file-nominee/{{ $nominee->id }}" style="display:inline-block;">
      @csrf
      {{ method_field('DELETE') }}
      <button class="btn btn-danger">Removed</button>
   </form>   --}}

 
</div>
 
 
       <div class="row">


         <div class="col-md-6">
    <div class="panel panel-default" >
       <div class="panel-heading" style="background:">
         <h4 class="panel-title"> Student Profile</h4>
          <div class="panel-heading-btn">
             
          </div>
       </div>
       <div class="panel-body">
         <table class="table table-bordered table-hover">
            <tbody>
               <tr>
                  <td>Student ID : {{ $user->student->student_no ?? null }}</td>
                  <td>FullName : {{ $user->student->firstname ?? null }} {{ $user->student->middlename ?? null }} {{ $user->student->lastname ?? null}}</td>
               </tr>
               <td>Address : {{ $user->student->address ?? null }}</td>
               <td>Contact No: {{ $user->student->contact_no ?? null }}</td>
               <tr>
                  <td>Email : {{ $user->email ?? null}}</td>
               </tr>

            </tbody>
         </table>
       </div>
    </div>
 </div>
           
 <div class="col-md-6">
    <div class="panel panel-default" >
       <div class="panel-heading" style="background:">
         <h4 class="panel-title"> Year Level</h4>
          <div class="panel-heading-btn">
             
          </div>
       </div>
       <div class="panel-body">
            <table class="table table-bordered table-hover" id="example">
               <thead>
                  <tr>
                  <th>Course</th>
                  <th>Year Level</th>
                  <th>Status</th>
                  <th>Options</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
            @forelse ($user->student->assessments as $assessment)
            <tr>
               <td>{{ $assessment->course->code }}</td>
               <td>
                  @if ($assessment->year == 1)
                  1st Year
                  @elseif ($assessment->year == 2)
                  2nd Year
                  @elseif ($assessment->year == 3)
                  3rd Year
                  @elseif ($assessment->year == 4)
                  4th Year
                  @elseif ($assessment->year == 5)
                  5th Year
                  @endif
               </td>
                <td>
            @if ($assessment->status == 'For Payment')
              <span class="label label-warning"> {{ $assessment->status }}</span>
            @elseif ($assessment->status == 'Paid')
              <span class="label label-success">{{ $assessment->status }}</span>
            @elseif ($assessment->status == 'Verified')
              <span class="label label-info">{{ $assessment->status }}</span>
            @elseif ($assessment->status == 'For Partial')
              <span class="label label-info">{{ $assessment->status }}</span>
            @endif
          </td>
                <td>
                {{--    <a href="{{ route('students.levels.subjects',[ $student, $level]) }}" class="btn btn-inverse btn-xs">Subjects</a> --}}
                  <a href="/student/assessment/{{ $assessment->id }}/subject" class="btn btn-primary btn-xs">Show Subjects</a>
                </td>
            </tr>
             @empty
            <tr>
               <td colspan="4" class="text-center">No available levels</td>
            </tr>
            @endforelse
               </tbody>
               
            </table>
         </div>
       </div>
   </div>
</div>


</div>













@endsection