@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Subject</h1>
            <!-- end page-header -->

  <div class="row">
  <div class="col-xl-3 col-md-6">
  <div class="widget widget-stats bg-primary" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
  <div class="stats-icon">
  <div class="icon-img">
  </div>
  </div>
  <div class="stats-info">
  <h4>AMOUNT</h4>
  <p>&#8369; {{ $assessment->totalAmount() }} </p>
  <small> </small>
  </div>
  <div class="stats-link">
  </div>
  </div>
  </div>
  <div class="col-xl-3 col-md-6">
  <div class="widget widget-stats bg-primary" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
  <div class="stats-icon">
  <div class="icon-img">
  </div>
  </div>
  <div class="stats-info">
  <h4>BALANCE</h4>
  <p>&#8369;  {{ abs($assessment->payAmount()) }} </p>
  <small> </small>
  </div>
  <div class="stats-link">
  </div>
  </div>
  </div>
  </div>


    <div class="row">


 
<div class="col-sm-6 col-xl-6">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Subjects</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
         <table class="table table-bordered">
            <thead>
              <tr>
                <th>Code</th>
                <th>Title</th>
                <th>Unit</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($assessment->subjectFees as $subject)
              <tr>
                <td>{{ $subject->code }}</td>
                <td>{{ $subject->name }}</td>
                <td>{{ $subject->unit }}</td>
                <td>{{ $subject->amount }}</td>
              </tr>
              @endforeach
             
            </tbody>
          </table>
         
      </div>
      </div>
      </div>

          <div class="col-sm-6 col-xl-6">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Transaction logs</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
        

        <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Transaction Amount</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($assessment->amounts as $assessment)
            <tr>
              <td>{{ $assessment->id }}</td>
              <td>{{ $assessment->amount }}</td>
              <td>{{ $assessment->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
         
      </div>
      </div>
      </div>


@endsection