@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/employees">Employee Users</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Employee Users </h1>
            <!-- end page-header -->
            <!-- begin panel -->

            <div class="form-group">
               <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Add Employee</a>
            </div>

     @include ('employees.modal.index')

         <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Add Employee User</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Fullname</th>
                           <th>Employee ID</th>
                           <th>Role</th>
                           <th>Options</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($employees as $employee)
                        <tr>
                           <td>{{ $employee->id }}</td>
                           <td>{{ $employee->name }}</td>
                           <td>{{ $employee->employee_no }}</td>
                           <td>{{ $employee->user->role->name ?? null }}</td>
                           <td>
                              <a href="/employees/{{ $employee->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
                              <a href="/employees/{{ $employee->id }}/show" class="btn btn-success btn-xs">Manage</a>
                              <!-- <form method="POST" action="/employees/{{ $employee->id }}" style="display:inline-block;">
                                 @csrf
                                 {{ method_field('DELETE') }}
                                 <button class="btn btn-danger btn-xs">Delete</button>
                              </form> -->
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection