@extends('layouts.master')
@section('content')


<div id="content" class="content content-full-width">

<div class="profile">
<div class="profile-header">

<div class="profile-header-cover"></div>

<div class="profile-header-content" id="particles-js">

   <div class="profile-header-img" >
     
      @if ($employee->avatar)
      <img src="{{ Storage::url($employee->avatar) }}" alt="" style="height: 100%; width: 100%"> 
      @else
      <img src="{{ asset('img/admin.png') }}" alt="" style="height: 100%" style="width: 100%">
      @endif
   </div>

<div class="profile-header-info" >
<h4 class="mt-0 mb-1">{{ $employee->name }}</h4>

</p>
<br>
<br>
</div>
</div>


<ul class="profile-header-tab nav nav-tabs" style="background: #f0f8ff21">
<li class="nav-item"><a class="nav-link active" data-toggle="tab" style="color: #fff;"></a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
<li class="nav-item"><a  class="nav-link" data-toggle="tab">&nbsp;</a></li>
</ul>
</div>
</div>
<div class="profile-content"> 
   <div class="form-group">
   @if ($employee->status == 0)
      <form method="POST" action="/employee/{{ $employee->id }}/enable-employee" style="display:inline-block;">
            {{ method_field('PATCH') }}
            @csrf
         <button type="submit" class="btn btn-success">Enable Certificate of Employee</button>
      </form>
      @endif
{{--    <form method="POST" action="/removed-request-file-nominee/{{ $nominee->id }}" style="display:inline-block;">
      @csrf
      {{ method_field('DELETE') }}
      <button class="btn btn-danger">Removed</button>
   </form>   --}} 
   @if ($employee->status == 1)
    <form method="POST" action="/employee/{{ $employee->id }}/disable-employee" style="display:inline-block;">
            {{ method_field('PATCH') }}
            @csrf
         <button type="submit" class="btn btn-danger">Disable Certificate of Employee</button>
      </form>
 @endif
 </div> 


<div class="tab-content p-0">
<div class="tab-pane fade show active" id="profile-post">
 <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Employee Profile </h4>
         <div class="panel-heading-btn">
   {{--          <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> --}}
         </div>
      </div>
      <div class="panel-body">

         <table class="table table-bordered">
            <tbody>
               <tr>
                  <td>Email : {{ $employee->user->email ?? null }}</td>
                  <td>Employee ID : {{ $employee->employee_no }}</td>
                  <td>Date of Join : {{ $employee->joining_date }}</td>
               </tr>
               <tr>
                  <td>Phone : {{ $employee->phone }}</td> 
                  <td>Birthdate : {{ $employee->birthday }}</td>
               </tr>
               <tr>
                  <td>Address : {{ $employee->address }}</td>
                  <td colspan="2">Gender :{{ $employee->gender }}</td>
               </tr>
            </tbody>
           
         </table>

      </div>
   </div>
</div>
</div>
</div>

@endsection