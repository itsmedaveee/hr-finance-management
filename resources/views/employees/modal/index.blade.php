 <div id="add_employee" class="modal custom-modal fade" role="dialog">
         <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title">Add Employee</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form method="POST" action="/employees" enctype="multipart/form-data">
                     @csrf
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Full Name <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="name">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Email <span class="text-danger">*</span></label>
                              <input class="form-control" type="email" name="email">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Username <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="username">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Password</label>
                              <input class="form-control" type="password" name="password">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Employee ID <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="employee_no">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Joining Date <span class="text-danger">*</span></label>
                              <input class="form-control " type="date" name="joining_date">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Phone </label>
                              <input class="form-control" type="text" name="phone">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Address </label>
                              <input class="form-control" type="text" name="address">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Birthdate </label>
                              <input class="form-control" type="date" name="birthday">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="col-form-label">Upload Photo </label>
                              <input class="form-control" type="file" name="avatar">
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Gender <span class="text-danger">*</span></label>
                              <select class="select form-control" name="gender">
                                 <option value="Select Gender" selected="" disabled="">Select Gender</option>
                                 <option>Male</option>
                                 <option>Female</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Select Department <span class="text-danger">*</span></label>
                              <select class="select form-control" name="departments">
                                 <option value="Select Department" selected="" disabled="">Select Department</option>
                                 @foreach ($departments as $department)
                                 <option value="{{ $department->id }}">{{ $department->name }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>                        

                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Select Schedule <span class="text-danger">*</span></label>
                              <select class="select form-control" name="schedules">
                                   <option value="Select Schedule" selected="" disabled="">Select Schedule</option>
                                 @foreach ($schedules as $schedule)
                                 <option value="{{ $schedule->id }}"> {{ $schedule->label }}- FROM {{ $schedule->time_in }} TO {{ $schedule->time_out }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Rates <span class="text-danger">*</span></label>
                        <input type="text" name="rates" class="form-control">
                     </div>
                  </div>         
                   <div class="col-md-12">
                     <div class="form-group">
                        <label>Position <span class="text-danger">*</span></label>
                        <input type="text" name="positions" class="form-control">
                     </div>
                  </div>
 
                     </div>
                     <div class="submit-section">
                        <button class="btn btn-primary submit-btn">Submit</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>