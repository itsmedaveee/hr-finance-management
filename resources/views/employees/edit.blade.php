@extends('layouts.master')
@section('content')
<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
           
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Employee User </h1>
            <!-- end page-header -->
            <!-- begin panel -->
<div class="row">
<div class="col-md-12">
<div class="panel panel-default" >
   <div class="panel-heading">
      <h4 class="panel-title">Edit Employee User</h4>
      <div class="panel-heading-btn">
         
      </div>
   </div>
   <div class="panel-body">
   <form method="POST" action="/employee/{{ $employee->id }}" enctype="multipart/form-data">
      @csrf
      {{ method_field('PATCH') }} 
      <div class="row">
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Full Name <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="name" value="{{ $employee->name }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Email <span class="text-danger">*</span></label>
               <input class="form-control" type="email" name="email" value="{{ $employee->user->email }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Username <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="username" value="{{ $employee->user->username }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Password</label>
               <input class="form-control" type="password" name="password">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Employee ID <span class="text-danger">*</span></label>
               <input type="text" class="form-control" name="employee_no" value="{{ $employee->employee_no }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Joining Date <span class="text-danger">*</span></label>
               <input class="form-control " type="date" name="joining_date" value="{{ $employee->joining_date }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Phone </label>
               <input class="form-control" type="text" name="phone" value="{{ $employee->phone }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Address </label>
               <input class="form-control" type="text" name="address" value="{{ $employee->address }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Birthdate </label>
               <input class="form-control" type="date" name="birthday" value="{{ $employee->birthday }}">
            </div>
         </div>
         <div class="col-sm-6">
            <div class="form-group">
               <label class="col-form-label">Upload Photo </label>
               <input class="form-control" type="file" name="avatar">
            </div>
         </div>
         <div class="col-md-6">
            <div class="form-group">
               <label>Gender <span class="text-danger">*</span></label>
               <select class="select form-control" name="gender">
                  <option>Male</option>
                  <option>Female</option>
               </select>
            </div>
         </div>
      
         <div class="col-md-6">
            <div class="form-group">
               <label>Department <span class="text-danger">*</span></label>
               <select class="select form-control" name="departments">
                  @foreach ($departments as $deparment)
                  <option value="{{ $deparment->id }}" {{ $deparment->id == $employee->department_id ? 'selected' : '' }} >{{ $deparment->name }}</option>
                  @endforeach
               </select>
            </div>
         </div>
 


          <div class="col-md-6">
                           <div class="form-group">
                              <label>Select Schedule <span class="text-danger">*</span></label>
                              <select class="select form-control" name="schedules">
                                 @foreach ($schedules as $schedule)
                                 <option value="{{ $schedule->id }}"> {{ $schedule->label }}- FROM {{ $schedule->time_in }} TO {{ $schedule->time_out }}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Rates <span class="text-danger">*</span></label>
                    <input type="text" name="rates" class="form-control" value="{{ $employee->rates }}">
                </div>
            </div>  
      <div class="col-md-12">
                <div class="form-group">
                    <label>Position <span class="text-danger">*</span></label>
                    <input type="text" name="positions" class="form-control" value="{{ $employee->positions }}">
                </div>
            </div> 
      </div>


      <div class="submit-section">
         <button type="submit" class="btn btn-primary submit-btn">Update</button>
      </div>
   </form>
</div>
@endsection