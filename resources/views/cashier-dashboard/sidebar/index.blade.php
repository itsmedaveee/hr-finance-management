<div id="sidebar" class="sidebar">
   <!-- begin sidebar scrollbar -->
   <div data-scrollbar="true" data-height="100%">
      <!-- begin sidebar user -->
      <ul class="nav">
         <li class="nav-profile">
            <a href="javascript:;" data-toggle="nav-profile">
               <div class="cover with-shadow"></div>

              
	              <div class="image">
	                 <img src="{{ asset('img/admin.png') }}" alt="" /> 
	              </div>
	         

               <div class="info">
                  {{ auth()->user()->username }}
                  <small>{{ auth()->user()->role->name }}</small>
               </div>
            </a>
         </li>
      </ul>
      <!-- end sidebar user -->
      <!-- begin sidebar nav -->
      <ul class="nav">
         <li class="nav-header">Navigation</li>
      {{--    <li class="has-sub {{ Request::is('cashier/home') ? 'active' : '' }}">
            <a href="/cashier/home">
               <div class="icon-img">
             		<i class="fa fa-home"></i>
               </div>
               <span>Dashboard</span>
            </a>
         </li>

 --}}

         <li class="has-sub {{ Request::is('students-list') ? 'active' : '' }}  {{ Request::is('students-payment') ? 'active' : '' }} "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-user-md"></i>
               </div>
               <span>Students Payment</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('students-payment') ? 'active' : '' }}"><a href="/students-payment">Students Payment</a></li>
               <li class="{{ Request::is('students-list') ? 'active' : '' }}"><a href="/students-list">Students lists</a></li>
            </ul>
         </li>

         <li class="has-sub {{ Request::is('income-report') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-money"></i>
               </div>
               <span>Finance </span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('income-report') ? 'active' : '' }}"><a href="/income-report">Income Report</a></li>
            </ul>
         </li>


         <li class="has-sub  {{ Request::is('attendances') ? 'active' : '' }} {{ Request::is('attendance-report') ? 'active' : '' }}   "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
               <i class="fa fa-clock"></i>
               </div>
               <span>Attendance</span>
            </a>
            <ul class="sub-menu">   
               <li class="{{ Request::is('attendances') ? 'active' : '' }}"><a href="/attendances">Attendace </a></li>
                <li class="{{ Request::is('attendance-report') ? 'active' : '' }}"><a href="/attendance-report">Attendace Report</a></li>
            </ul>
         </li>       



         <li class="has-sub {{ Request::is('transaction-logs') ? 'active' : '' }}  "  style="border-bottom: 1px solid #46505a;
            border-top: 1px solid #46505a;">
            <a href="javascript:;">
               <b class="caret"></b>
               <div class="icon-img">
               {{--    <img src="{{ asset('img/app.png') }}" alt="" class="round bg-inverse" /> --}}
              <i class="fa fa-rub" aria-hidden="true"></i>
               </div>
               <span>Logs</span>
            </a>
            <ul class="sub-menu">
               <li class="{{ Request::is('transaction-logs') ? 'active' : '' }}"><a href="/transaction-logs">Transactions Logs</a></li>
            </ul>
         </li>


         <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-back"></i> <span>Collapse</span></a></li>
         <!-- end sidebar minify button -->
      </ul>
      <!-- end sidebar nav -->
   </div>
   <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>