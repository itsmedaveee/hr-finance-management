@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">For Payment</h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Students for Payment</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               	<table class="table table-bordered">
               		<thead>
               			<tr>
                           <th></th>
               				<th>FullName</th>
               				<th>Address</th>
               				<th>Course</th>
               				<th>Year</th>
               				<th>Status</th>
               				<th>Options</th>
               			</tr>
               		</thead>
               		<tbody>
               			@foreach ($assessments as $assessment)
               			<tr>
                           <td></td>
               				<td>{{ optional($assessment->student)->firstname }} {{ optional($assessment->student)->middlename }} {{ optional($assessment->student)->lastname }}</td>
               				<td>{{ optional($assessment->student)->address }}</td>
               				<td>{{ $assessment->student->latestAssessment->course->code ?? null }}
						   </td>
						   <td>
						@if ($assessment->student->latestAssessment ?? null )
						    @if ($assessment->student->latestAssessment->year == 1)
						        1st Year
						    @elseif ($assessment->student->latestAssessment->year == 2)
						        2nd Year
						    @elseif ($assessment->student->latestAssessment->year == 3)
						        3rd Year
						    @elseif ($assessment->student->latestAssessment->year == 4)
						        4th Year
						    @elseif ($assessment->student->latestAssessment->year == 5)
						        5th Year
						    @endif
						@else
						    Please assign a course/level!
						@endif
						   </td>
						   <td>
						   	<span class="label label-info">{{ $assessment->status }}</span>
						   </td>
						   <td>
						   	<a href="/cashier/student-payment/{{ $assessment->id }}/manage" class="btn btn-primary btn-xs">Manage</a>
						   </td>
               			</tr>
               			@endforeach
               		</tbody>
               	</table>
               </div>
           </div>
       </div>
   </div>
</div>








@endsection