@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
<ol class="breadcrumb float-xl-right">
    
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Students</h1>
<!-- end page-header -->
<!-- begin panel -->
<div class="row">
<div class="col-md-12">
<div class="panel panel-default" >
   <div class="panel-heading">
      <h4 class="panel-title">Students lists</h4>
      <div class="panel-heading-btn">
         
      </div>
   </div>
   <div class="panel-body">

   		<table class="table table-bordered">
   			<thead>
   				<tr>
   					<th>Student ID</th>
   					<th>FullName</th>
   					<th>Status</th>
   					<th>Options</th>
   				</tr>
   				<tbody>
   					@foreach ($assessments as $assessment)
   					<tr>
   						<td>{{ $assessment->student->student_no }}</td>
   						<td>{{ $assessment->student->firstname }} {{ $assessment->student->middlename }} {{ $assessment->student->lastname }}</td>
   						<td><span class="label label-success"> {{ $assessment->status }}</span></td>
   						<td><a href="/students/{{ $assessment->id }}/show" class="btn btn-primary btn-xs">Show </a></td>
   					</tr>
   					@endforeach
   				</tbody>
   			</thead>
   		</table>


   </div>
</div>
</div>
</div>


















@endsection