@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
         
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Student Details</h1>
            <!-- end page-header -->
            <!-- begin panel -->
     <div class="row">
            <div class="col-md-6">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Student Details</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               	<table class="table table-hover table-bordered m-0">
                    <thead>
                        <tr>
                            <th>Student ID</th>
                            <th>FullName</th>
                            <th>Address</th>
                            <th>Contact No</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                    		<td>{{ $assessment->student->student_no }}</td>
                        <td>
                          {{ $assessment->student->firstname }}
                          {{ $assessment->student->middlename }}
                          {{ $assessment->student->lastname }}
                        </td>
                    		<td>{{ $assessment->student->address }}</td>
                    		<td>{{ $assessment->student->contact_no }} </td>
                    	</tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>

         <div class="col-md-6">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Year/Course </h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">

               	<table class="table table-bordered table-hover">
               		<thead>
               			<tr>
               				<th>Course</th>
               				<th>Year</th>
               				<th>Semister</th>
               				<th>Status</th>
               				<th>Option</th>
               			</tr>
               		</thead>
               		<tbody>
               			<tr>
               				<td>{{ $assessment->course->code }}</td>
               				<td>{{ $assessment->year }}</td>
               				<td>{{ $assessment->semister }}</td>
               				<td>

               					@if ($assessment->status == 'Paid')
               						<span class="label label-success">{{ $assessment->status }}</span>
               					@endif

               				</td>
               				<td><a href="/cashier/student-payment/{{ $assessment->id }}/manage" class="btn btn-primary btn-xs">Manage</a></td>
               			</tr>
               		</tbody>
               	</table>

               </div>
           </div>
       </div>
   </div>























@endsection