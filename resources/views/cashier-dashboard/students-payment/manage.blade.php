@extends('layouts.master')
@section('content')


<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Manage Student Payment</h1>
            <!-- end page-header -->
            <!-- begin panel -->


  <div class="row">
  <div class="col-xl-3 col-md-6">
  <div class="widget widget-stats bg-primary" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
  <div class="stats-icon">
  <div class="icon-img">
  </div>
  </div>
  <div class="stats-info">
  <h4>AMOUNT</h4>
  <p>&#8369; {{ $assessment->totalAmount() }} </p>
  <small> </small>
  </div>
  <div class="stats-link">
  </div>
  </div>
  </div>
  <div class="col-xl-3 col-md-6">
  <div class="widget widget-stats bg-primary" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
  <div class="stats-icon">
  <div class="icon-img">
  </div>
  </div>
  <div class="stats-info">
  <h4>BALANCE</h4>
  <p>&#8369;  {{ abs($assessment->payAmount()) }} </p>
  <small> </small>
  </div>
  <div class="stats-link">
  </div>
  </div>
  </div>
  </div>


    <div class="row">


      <div class="col-sm-12 col-xl-12">
        <a href="/pdf-cor-generate/{{ $assessment->student->latestAssessment->id ?? null }}" class="btn btn-sm btn-info m-b-10" target="_blank" ><i class="fa fa-print"></i> Print</a> 

      <div class="panel panel-default" >
       <div class="panel-heading">
          <h4 class="panel-title">Student Info</h4>
          <div class="panel-heading-btn">
             
          </div>
       </div>
       <div class="panel-body">
       	<table class="table table-bordered">

       		<thead>
       			<tr>
       				<th>FullName</th>
                   <th>Course</th>
                   <th>Year</th>
                   <th>Status</th>
       			</tr>
       		</thead>
       		<tbody>
       			<td>{{ $assessment->student->firstname ?? null }} {{ $assessment->student->middlename ?? null }} {{ $assessment->student->lastname ?? null }}</td>
                <td>{{ $assessment->student->latestAssessment->course->code ?? null}}</td>
                <td>
                      @if ($assessment->student->latestAssessment ?? null)
              @if ($assessment->student->latestAssessment->year == 1)
                  1st Year
              @elseif ($assessment->student->latestAssessment->year == 2)
                  2nd Year
              @elseif ($assessment->student->latestAssessment->year == 3)
                  3rd Year
              @elseif ($assessment->student->latestAssessment->year == 4)
                  4th Year
              @elseif ($assessment->student->latestAssessment->year == 5)
                  5th Year
              @endif
          @else
              Please assign a course/level!
          @endif
             </td>
             <td>
              @if ($assessment->status == 'For Payment')
                <span class="label label-warning">{{ $assessment->status }}</span>
              @endif
              @if ($assessment->status == 'For Partial')
                <span class="label label-info">{{ $assessment->status }}</span>
              @endif
             </td>
               
       		</tbody>
       		
       	</table>
       </div>
      </div>
      </div>



   {{--    <div class="col-sm-12 col-xl-12">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Add Payment</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
        <form method="POST" action="/cashier/student-payment/{{ $assessment->id }}">
          @csrf
          {{ method_field('PATCH') }}
          <label>Amount</label>
          <div class="form-group">
            <input type="text" name="amount" class="form-control">
           </div>
           <div class="form-group">

            <label>Status</label>

                <select class="form-control" name="status">
                  <option selected="" disabled="">Select Status</option>
                  <option value="For Partial">For Partial</option>
                  <option value="Paid">Paid</option>
                </select>
           </div>

           <div class="form-group">
             <button type="submit" class="btn btn-primary">Update</button>
           </div>
          
        </form>
         
      </div>
      </div>
      </div> --}}


<div class="col-sm-3 col-xl-3">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Add Payment</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
        <form method="POST" action="/cashier/student-payment/{{ $assessment->id }}">
          @csrf
          {{ method_field('PATCH') }}
          <label>Amount</label>
          <div class="form-group">
            <input type="text" name="amount" class="form-control">
           </div>
           <div class="form-group">

            <label>Status</label>

                <select class="form-control" name="status">
                  <option selected="" disabled="">Select Status</option>
                  <option value="For Partial">For Partial</option>
                  <option value="Paid">Paid</option>
                </select>
           </div>

           <div class="form-group">
             <button type="submit" class="btn btn-primary">Update</button>
           </div>
          
        </form>
         
      </div>
      </div>
      </div>

          <div class="col-sm-5 col-xl-5">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title"> Subject List</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
         <table class="table table-bordered">
            <thead>
              <tr>
                <th>Code</th>
                <th>Title</th>
                <th>Unit</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($assessment->subjectFees as $subject)
              <tr>
                <td>{{ $subject->code }}</td>
                <td>{{ $subject->name }}</td>
                <td>{{ $subject->unit }}</td>
                <td>{{ $subject->amount }}</td>
              </tr>
              @endforeach
             
            </tbody>
          </table>
         
      </div>
      </div>
      </div>

          <div class="col-sm-4 col-xl-4">
      <div class="panel panel-default">
      <div class="panel-heading">
         <h4 class="panel-title">Transaction logs</h4>
         <div class="panel-heading-btn">

         </div>
      </div>
      <div class="panel-body ">
        

        <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Transaction Amount</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($assessment->amounts as $assessment)
            <tr>
              <td>{{ $assessment->id }}</td>
              <td>{{ $assessment->amount }}</td>
              <td>{{ $assessment->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
         
      </div>
      </div>
      </div>

      



@endsection