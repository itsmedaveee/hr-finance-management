@extends('layouts.master')
@section('content')
<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					 
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">Transaction logs </h1>


			<div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Logs</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               	<table class="table table-bordered">
               		<thead>
               			<tr>
               				<th>ID</th>
               				<th>User</th>
               				<th>Amount</th>
               			</tr>
               		</thead>
               		<tbody>
               			@foreach ($amounts as $amount)
               			<tr>
               				<td>{{ $amount->id }}</td>
               				<td>{{ $amount->user->username }}</td>
               				<td>{{ $amount->amount }}</td>
               			</tr>
               			@endforeach
               		</tbody>
               	</table>

               </div>
           </div>
       </div>
   </div>
</div>






@endsection