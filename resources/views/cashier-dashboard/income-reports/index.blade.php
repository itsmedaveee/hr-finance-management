@extends('layouts.master')
@section('content')
<div id="content" class="content">
	<h1 class="page-header ">Income Report</h1>


@if (request()->has('month') )
<div class="row">
<div class="col-xl-3 col-md-6">
<div class="widget widget-stats bg-teal" style="background: linear-gradient(to right, #ff9b44 0%, #fc6075 100%)">
<div class="stats-icon stats-icon-lg"><i class="fa fa-money fa-fw"></i></div>
<div class="stats-content">
<div class="stats-title">Total Amount </div>
<div class="stats-number">

	@foreach ($assessments as $assessment)
		@if ($loop->last)
			{{ $assessment->sum('amount') }}
		@endif
	 @endforeach
</div>
<div class="stats-progress progress">
<div class="progress-bar" style="width: 70.1%;"></div>
</div>
<div class="stats-desc"><br></div>
</div>
</div>
</div>
</div>
	@endif
	<div class="row">

	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">-</h4>
				<div class="panel-heading-btn">
				
				</div>
			</div>
		
			<div class="panel-body ">
						<form class="form-horizontal" method="GET" action="/income-report"> 
						<div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
						    <label for="from" class="col-md-4 control-label ">Select Monthly</label>
						    <div class="col-md-12">
						        <input id="from" type="month" class="form-control " name="month" value="{{ request('month') }}">

						        @if ($errors->has('from'))
						            <span class="help-block">
						                <strong style="color: #ff5b57;">{{ $errors->first('month') }}</strong>
						            </span>
						        @endif
						    </div>
						</div>


						<div class="form-group">
						    <div class="col-md-6 col-md-offset-4">
						        <button type="submit" class="btn btn-outline-primary">
						            Generate
						        </button>

						        <a href="" class="btn btn-outline-danger">
						            Cancel
						        </a>
						    </div>
						</div>
						</form>
					</div>
				</div>
				</div>

		<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">-</h4>
				
			</div>

			<div class="panel-body ">

			<div class="form-group">
				@if (request()->has('month') )
				  <a href="{{ route('monthly-report/export', ['month' => request('month') ]) }}" class="btn btn-outline-primary" style="display: inline-block;">Generate PDF Reports</a>
				@endif
				</div>
				<table class="table table-bordered">
			    	<thead>
			    		<th>Date</th>
			    		<th>Amount</th>
			    	</thead>
			    	@if (request()->has('month'))
			    	<tbody>
			    		@foreach ($assessments as $assessment)
			    		<tr>
			    			<td >{{ $assessment->created_at->toFormattedDateString() }}</td>
			    			<td >{{ $assessment->amount }}</td>
			    		</tr>

			    		@if ($loop->last)
			    		<tr>
			    			<td colspan="1"></td>
			    			<td>Total : {{ $assessment->sum('amount') }}</td>
			    		</tr>
			    		@endif
			    	
			    		@endforeach
			    			
			    	</tbody>
			    	@endif
			    </table>

			    

			</div>
		</div>
	</div>
</div>







@endsection