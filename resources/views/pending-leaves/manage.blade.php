@extends('layouts.master')
@section('content')

<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Manage Leave</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Leaves</a></li>
                  <li class="breadcrumb-item active">Leaves</li>
               </ul>
            </div>
         </div>
      </div>

		<form method="POST" action="/manage-update/{{ $leave->id }}" style="display:inline-block;">
		@csrf
		{{ method_field('PATCH') }}
		<button type="submit" class="btn btn-primary btn-sm">Approved</button>
		</form>            					
		<form method="POST" action="/manage-cancel/{{ $leave->id }}" style="display:inline-block;">
		@csrf
		{{ method_field('PATCH') }}
		<button type="submit" class="btn btn-danger btn-sm">Cancel</button>
		</form>
		<br>
		<br>
 
      <div class="row">

         <div class="col-sm-12">

            <div class="card mb-0">

               <div class="card-header">
                  <h4 class="card-title mb-0">Manage Leave </h4>
               </div>
               <div class="card-body">

               	<table class="table table-bordered">
               		<thead>
               			<tr>
               				<th>ID</th>
               				<th>Name</th>
               				<th>Types Leave</th>
               				<th>Reason</th>
               				<th>No. of days</th>
               				<th>Date</th>
               				<th>Status</th>
               			</tr>
               		</thead>
               		<tbody>
               			<tr>
               				<td>{{ $leave->id }}</td>
               				<td>{{ $leave->user->email }}</td>
               				<td>{{ $leave->types_leave }}</td>
               				<td>{{ $leave->reason }}</td>
               				<td>{{ $leave->no_of_days }}</td>
               				<td>{{ $leave->date }}</td>
               				<td>{{ $leave->status }}</td>
               			
               			</tr>
               		</tbody>
               	</table>

               </div>
           </div>
       </div>
   </div>
</div>
</div>

 


@endsection