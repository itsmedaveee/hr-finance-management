@extends('layouts.master')
@section('content')

 
<div class="page-wrapper">
   <div class="content container-fluid">
      <div class="page-header">
         <div class="row align-items-center">
            <div class="col">
               <h3 class="page-title">Pending Leaves</h3>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Leaves</a></li>
                  <li class="breadcrumb-item active">Leaves</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div class="card mb-0">
               <div class="card-header">
                  <h4 class="card-title mb-0">Pending Leaves</h4>
               </div>
               <div class="card-body">

               	<table class="table table-bordered">
               		<thead>
               			<tr>
               				<th>ID</th>
               				<th>Name</th>
               				<th>Status</th>
               				<th>Actions</th>
               			</tr>
               		</thead>
               		<tbody>
               			@foreach ($leaves as $leave)
               			<tr>
               				<td>{{ $leave->id }}</td>
               				<td>{{ $leave->user->email }}</td>
               				<td>{{ $leave->status }}</td>
               				<td><a href="/manage-pending-leaves/{{ $leave->id }}/" class="btn btn-primary btn-xs">Manage</a></td>
               			</tr>
               			@endforeach
               		</tbody>
               	</table>

               </div>
           </div>
       </div>
   </div>
</div>
</div>




@endsection