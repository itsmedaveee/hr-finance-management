@extends('layouts.master')
@section('content')

<div id="content" class="content">
            <!-- begin breadcrumb -->
            <ol class="breadcrumb float-xl-right">
               <li class="breadcrumb-item"><a href="/home">Home</a></li>
               <li class="breadcrumb-item"><a href="/settings">Settings</a></li>
                
            </ol>
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Settings </h1>
            <!-- end page-header -->
            <!-- begin panel -->

       <div class="row">
            <div class="col-md-12">
            <div class="panel panel-default" >
               <div class="panel-heading">
                  <h4 class="panel-title">Settings</h4>
                  <div class="panel-heading-btn">
                     
                  </div>
               </div>
               <div class="panel-body">
               		<form method="POST" action="/settings">
               			@csrf
               		{{ method_field('PATCH') }}

               			 <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Username <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="username" value="{{ auth()->user()->username }}">
            </div>
         </div>     
          <div class="col-sm-12">      
          <div class="form-group">
               <label class="col-form-label">Email <span class="text-danger">*</span></label>
               <input class="form-control" type="text" name="email" value="{{ auth()->user()->email }}">
            </div>
         </div>
         <div class="col-sm-12">
            <div class="form-group">
               <label class="col-form-label">Password</label>
               <input class="form-control" type="password" name="password">
            </div>
      
     		
       			<div class="form-group">
       				<button type="submit" class="btn btn-primary ">Update</button>
       				
       			</div>
               		</form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection