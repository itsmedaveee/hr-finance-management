<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          //Administrator
        $user = User::create([
            'username'  => 'admin',
            'email'  => 'admin@yahoo.com',
            'password'  => bcrypt('123456')
        ]);

        $role = Role::firstOrCreate([
            'label'   => 'Administrator',
            'name'    => 'administrator',
        ]);

        $user->role()->associate($role)->save();
    }
}
