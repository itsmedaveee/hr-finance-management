<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0);
            $table->integer('department_id')->default(0);
            $table->string('name');
            $table->string('employee_no');
            $table->string('joining_date');
            $table->string('phone')->nullable();
            $table->string('avatar')->nullable();
            $table->string('gender');
            $table->string('rates');
            $table->string('address');
            $table->string('birthday');
            $table->string('positions');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
