<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelSubject extends Model
{
    protected $fillable  = [
        'level_id',
        'student_id',
        'subject_id',
    ];

 
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
