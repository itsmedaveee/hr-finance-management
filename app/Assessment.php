<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $fillable = [
    	'student_id',
    	'course_id',
    	'year',
    	'status',
    	'date',
        'semister',
        'school_year_to',
        'school_year_from',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }    

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
 	
 	public function subjectFees()
 	{
 		return $this->hasMany(SubjectFee::class);
 	}

 	public function totalAmount()
    {
        return $this->subjectFees->sum('amount');
    }

    public function totalUnit()
    {
        return $this->subjectFees->sum('unit');
    }

    public function amounts()
    {
        return $this->hasMany(Amount::class);
    }


    public function payAmount()
    {
        return $this->amounts->sum('amount') - $this->subjectFees->sum('amount');
    }

}
