<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectFee extends Model
{
    protected $fillable = [
    	'code',
    	'name',
    	'amount',
        'unit',
        'date',
    	'assessment_id'
    ];

    public function assessment()
    {
    	return $this->belongsTo(Assessment::class);
    }

    
}
