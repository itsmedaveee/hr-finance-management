<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Attendance extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'user_id',
    	'date',
    	'time_in',
		'time_out',
		'total',
        'status'
    ];

    protected $dates = [
        'time_in',
        'time_out',  
    ];

    public function employee()
    {
    	return $this->belongsTo(Employee::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getTimeStartAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setTimeStartAttribute($value)
    {
        $this->attributes['time_in'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getTimeEndAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setTimeEndAttribute($value)
    {
        $this->attributes['time_out'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getDateStartAttribute()
    {
        return $this->time_in ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_in)->format(config('panel.date_format')) : null;
    }

    public function getTotalTimeAttribute()
    {
        $time_in = $this->time_in ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_in) : null;
        $time_out = $this->time_out ? Carbon::createFromFormat('Y-m-d H:i:s', $this->time_out) : null;

        return $this->time_out ? $time_out->diffInSeconds($time_in) : 0;
    }

    public function getTotalTimeChartAttribute()
    {
        return $this->total_time ? round($this->total_time/3600, 2) : 0;
    }
}
