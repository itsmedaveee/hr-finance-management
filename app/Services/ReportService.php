<?php

namespace App\Services;

use App\Attendance;
use Carbon\CarbonPeriod;

class ReportService
{
    public function generateReport($user)
    {
        $user = auth()->user()->id;
        $dateRange = [
            now()->subDays(config('app.reports.range'))->setTime(0, 0, 0),
            now()->addDay()->setTime(0, 0, 0)
        ];

        return Attendance::whereNotNull('time_out')
            ->whereBetween('time_in', $dateRange)
            ->get()
            ->groupBy('date')
            ->map(function ($items) {
                $sum = 0;
                foreach($items as $item)
                {
                    $sum += $item->total_time;
                }

                return $sum;
            })
            ->toArray();
    }

    public function generateDateRange()
    {
        $period = CarbonPeriod::create(now()->subDays(config('app.reports.range')), now());
        $dateRange = [];

        foreach ($period as $date) {
            array_unshift($dateRange, $date->format('Y-m-d'));
        }

        return $dateRange;
    }
}
