<?php

namespace App\Exports;

use App\Attendance;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class AttendanceExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     $user = auth()->user()->load('attendances');
    
    //     $collection  = $user->attendances;

    //     $grouped     = $collection->groupBy('date');
    //     $attendances = $grouped->toArray();
 
    //     return  $collection;
    // }

   public function view(): View
    {
        $user = auth()->user()->load('attendances');

        $collection  = $user->attendances;
        $grouped     = $collection->groupBy('date');
        $attendances = $grouped->toArray();
        return view('attendances.reports.index', compact('attendances'));
    }
}
