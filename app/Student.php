<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'course_id',
        'firstname',
        'middlename',
        'lastname',
        'birthdate',
        'address',
        'contact_no',
        'student_no',
        'email',
        'avatar',
    ];

    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }

    public function latestAssessment()
    {
        return $this->hasOne(Assessment::class)->latest();
    }
    

    // public function level()
    // {
    //     return $this->belongsTo(Level::class);
    // }

    // public function levels()
    // {
    //     return $this->hasMany(Level::class);
    // }

    // public function latestLevel()
    // {
    //     return $this->hasOne(Level::class)->latest();
    // }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }


    // public function bills()
    // {
    //     return $this->hasManyThrough(LevelSubject::class, Level::class);
    // }

 
}
