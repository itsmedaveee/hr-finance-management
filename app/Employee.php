<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

	protected $fillable = [
		'name',
		'user_id',
		'department_id',
		'name',
		'employee_no',
		'joining_date',
		'phone',
		'avatar',
		'status',
		'gender',
		'rates',
		'address',
		'positions',
		'birthday',
	];

	protected $dates = ['joining_date'];
	
	public function schedules() 
	{
		return $this->belongsToMany(Schedule::class, 'employee_schedules', 'employee_id', 'schedule_id');
	}	

    public function user()
    {
    	return $this->belongsTo(User::class);
    }    

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }

    public function attendances()
    {
    	return $this->hasMany(Attendance::class);
    }

    public function schedule()
    {
    	return $this->belongsTo(Schedule::class);
    }

}
