<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [

        'code',
        'name',
        'amount',
        // 'course_id',
        'year',
        'unit'
    ];

    // public function course()
    // {
    //     return $this->belongsTo(Course::class);
    // }


    public function level()
    {
        return $this->belongsTo(Level::class);
    }


    public function subjectFees()
    {
        return $this->hasMany(SubjectFee::class);
    }



}
