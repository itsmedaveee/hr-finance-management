<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = [
        'user_id',
        'types_leave',
        'duration',
        'date',
        'reason',
        'no_of_days',
        'status'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
