<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $guarded = [];


    public function employees() 
    {
        return $this->belongsToMany(Employee::class, 'employee_schedules', 'employee_id', 'schedule_id');
    }   

}


