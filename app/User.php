<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'student_id', 'username', 'email', 'password', 'employee_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() 
    {
        return $this->belongsTo(Role::class);
    }
    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function leaves()
    {
        return $this->hasMany(Leave::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    

    public function isAdmin()
    {
        return auth()->check() && auth()->user()->role->name == 'Administrator';
    }    
    public function isFinance()
    {
        return auth()->check() && auth()->user()->role->name == 'Finance';
    }   
    public function isRegistrar()
    {
        return auth()->check() && auth()->user()->role->name == 'Registrar';
    }    
    public function isCashier()
    {
        return auth()->check() && auth()->user()->role->name == 'Cashier';
    }    
    public function isStudent()
    {
        return auth()->check() && auth()->user()->role->name == 'Student';
    }
    public function hasRole($role)
    {
        return in_array($this->role->name, $role);
    }

}
