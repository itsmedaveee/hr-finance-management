<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;

class SchedulesController extends Controller
{
    public function index()
    {
        $schedules = Schedule::all();


        return view('schedules.index', compact('schedules'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'label' => 'required',
            'time_in'   => 'required',
            'time_out'   => 'required',
        ]);

        $timeIn = new \Carbon\Carbon($request['time_in']);

        Schedule::create([
            'label'     => request('label'),
            'time_in'   => $timeIn,
            'time_out'   => request('time_out'),
        ]);


        return back()->with('success', 'Schedule has been added!');
    }

    public function destroy(Schedule $schedule) 
    {
        $schedule->delete();

        return back()->with('error', 'Schedule has been remove!');
    }
}
