<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;
Use App\Role;


class CashiersController extends Controller
{
    public function index()
    {
        $cashiers = User::where('role_id', 3)->get();
        return view('cashiers.index', compact('cashiers'));
    }

    public function store(Request $request)
    {
       $role  = Role::where('label', 'cashier')->first();
       $user  = User::create([
            'username'  => $request->username,
            'password'  => bcrypt($request->password),
            'email'     => $request->email
        ]);

       $user->role()->associate($role)->save();
       return back()->with('success', 'Cashier has been added');
    }
}
