<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessment;
class StudentDashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user()->load('student.latestAssessment');


        return view('students-dashboard.index', compact('user'));
    }

    public function show(Assessment $assessment)
    {
    	return view('students-dashboard.show', compact('assessment'));
    }

}
