<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CoursesController extends Controller
{
    public function index()
    {
        $courses = Course::all();

        return view('courses.index', compact('courses'));
    }

    public function store(Request $request)
    {
        $this->validate(request(),[

            'code'       =>  'required',
            'title'      =>  'required',

        ]);

        $course = Course::create([

            'code'    =>  $request->code,
            'title'   =>  $request->title

        ]);

        session()->flash('success', "Course: {$course->code} Added!");

        return back();
    }

    public function edit(Course $course)
    {
        return view('courses.edit', compact('course'));
    }

    public function delete(Course $course)
    {
        $course->delete();

        return back()->with('error', 'Course has been removed!');
    }

    public function update(Request $request, Course $course)
    {
        $this->validate(request(),[

            'code'       =>  'required',
            'title'      =>  'required',

        ]);


        $course->update([

            'code'    =>  $request->code,
            'title'   =>  $request->title

        ]);

         session()->flash('info',  "Course: {$course->code} Updated!");

        return redirect('/courses');
    }

    public function destroy(Course $course)
    {
        $course->delete();

        return back()->with('error', 'Course has been removed!');
    }
}
