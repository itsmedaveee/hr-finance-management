<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
class AdminController extends Controller
{
    public function index()
    {
        $admins = User::where('role_id', 1)->get();
        return view('admin.index', compact('admins'));
    }

    public function store(Request $request)
    {   
        $this->validate(request(), [
            'username'  => 'required',
            'email'  => 'required',
            'password'  => 'required',
        ]);


       $role  = Role::where('label', 'administrator')->first();
       $user  = User::create([
            'username'  => $request->username,
            'password'  => bcrypt($request->password),
            'email'     => $request->email
        ]);

       $user->role()->associate($role)->save();
       return back()->with('success', 'Admin has been added!');
    }

    public function edit(User $admin)
    {
        return view('admin.edit', compact('admin'));
    }

    public function update(User $admin, Request $request)
    {
        $this->validate(request(), [
            'username'  => 'required',
            'email'  => 'required', 
        ]); 
       $role  = Role::where('label', 'administrator')->first();

        if ($request->password != null) { 
            auth()->user()->update([
                'password'           => bcrypt(request('password'))

            ]);
        } 
        
       $admin->update([
            'username'  => $request->username, 
            'email'     => $request->email
        ]);

       $admin->role()->associate($role)->save();
       return redirect('/admin-users')->with('info', 'Admin has been updated!');
    }
}
