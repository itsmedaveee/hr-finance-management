<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leave;


class PendingLeavesController extends Controller
{
    public function pendingLeaves()
    {      
        
        $leaves = Leave::with('user.department')->where('status', 'Pending')->get();

        return view('pending-leaves.index', compact('leaves'));
    }

    public function manage(Leave $leave)
    {
        return view('pending-leaves.manage', compact('leave'));
    }

    public function update(Request $request, Leave $leave)
    {
        $leave->update([

            'status'    => 'Approved',
        ]);


        return redirect('/pending-leaves');
    }

    public function cancel(Request $request, Leave $leave)
    {
        $leave->update([

            'status'    => 'Cancel',
        ]);


        return redirect('/pending-leaves');
    }
}
