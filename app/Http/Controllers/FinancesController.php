<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class FinancesController extends Controller
{
    public function index()
    {
        $finances = User::where('role_id', 2)->get();

        return view('finances.index', compact('finances'));
    }

    public function store(Request $request)
    {
       $role  = Role::where('label', 'finance')->first();
       $user  = User::create([
            'username'  => $request->username,
            'password'  => bcrypt($request->password),
            'email'     => $request->email
        ]);

       $user->role()->associate($role)->save();
       return back();
    }
}
