<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Assessment;
use App\Student;
use App\Amount;
class CashiersDashboardController extends Controller
{
    public function index()
    {
    	return view('cashier-dashboard.index');
    }

    public function studentsPending()
    {
		$assessments = Assessment::with('student')	
                                ->where('status', 'For Payment')
                                ->orWhere('status', 'For Partial')
                                ->get();

    	return view('cashier-dashboard.students-payment.index', compact('assessments'));
    }



    public function manageStudent(Assessment $assessment)
    {
    	 // $assessment->load('student', 'subjectFees');
    	 $assessment->load('student', 'subjectFees', 'amounts.assessment');

    	 $amounts = $assessment->amounts;
    	 
       // foreach ($amounts  as $assessment) {

       // 		 $assessment->amount->sum('amounts') 

       // }

    



    	  	
    	return view('cashier-dashboard.students-payment.manage', [

    		'assessment'	=> $assessment,
    		'amounts'	=> $amounts,
    	
    	]);
    }

    public function update(Assessment $assessment)
    {
    	$assessment->update([

    		'status'	=> request('status')
    	]);

    	$amount = Amount::create([
    		'amount'	=> request('amount'),
            'user_id'   => auth()->user()->id
    	]);


    	$amount->assessment()->associate($assessment)->save();

    	return back()->with('info', 'Payment has been added!');
    }

    public function studentList()
    {
        $assessments = Assessment::with('student')->where('status', 'Paid')->get();

        return view('cashier-dashboard.students-payment.student-list', compact('assessments'));
    }

    public function studentShow(Assessment $assessment)
    {
        return view('cashier-dashboard.students-payment.show', compact('assessment'));
    }
}
