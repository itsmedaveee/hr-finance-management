<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view('settings.index');
    }

    public function update(Request $request)
    {
        $this->validate(request(),[

            'email'                 => 'required',
            'username'              => 'required',
            'password'              => 'required',

        ]);

        if ($request->password != null) { 
            auth()->user()->update([
                'password'           => bcrypt(request('password'))

            ]);
        }


        $student  = $request->avatar;
        if (request()->hasFile('avatar')) {
            $file      = $request->file('avatar');
            $extension = $file->extension();
            $filename  = time() . '-' . str_slug($student) . '.' . $extension;
            $path      = $file->storeAs('public/avatar', $filename);
            // auth()->user()->update([
            //     'avatar'  => $path
            // ]);

            $student->update([
                'avatar'    => $path
            ]);

        }
 
       $user = auth()->user()->update([

            'name'          => $request->name,
            'email'         => $request->email,
            'username'      => $request->username,
            
        ]);

        return back()->with('info', 'Settings has been updated!');  
    }
}
