<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amount;
class LogsController extends Controller
{
    public function index()
    {
    	$amounts = Amount::with('user')->get();
    	return view('cashier-dashboard.transaction-logs.index', compact('amounts'));
    }
}
