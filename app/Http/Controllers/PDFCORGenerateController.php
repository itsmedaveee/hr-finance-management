<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessment;

class PDFCORGenerateController extends Controller
{
    public function index(Assessment $assessment)
    {

		$assessment->load(['student','subjectFees']);


    	$pdf        = \PDF::loadView('pdf.index', [
            'assessment'    => $assessment,
        ]);

        $pdf->setPaper('legal','portrait'); 
        
        return $pdf->stream(); 
    }
}
