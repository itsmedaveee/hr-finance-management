<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Subject;
use App\Level;


class StudSubjectController extends Controller
{
    public function show(Student $student, Level $level)
    {
         $level = $level->load('student', 'subjects.subject');

        $subjects = Subject::pluck('code', 'id');

          return view('students.subjects.show', [
            'student'  => $student,
            'subjects' => $subjects,
            'level'    => $level,
        ]);
    }


    public function update(Request $request, Student $student, Level $level)
    {

        $subject = $level->subject( 

          $request->subject_id, $student->id

        );

        return back();
    }


}
