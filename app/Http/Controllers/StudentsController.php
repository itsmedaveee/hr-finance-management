<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Student;
use App\Role;
use App\User;
use App\Course;
use Carbon\Carbon;
use App\Mail\SendingMail;
use Illuminate\Support\Facades\Mail;

class StudentsController extends Controller
{
    public function index()
    {
        $courses  = Course::pluck('title', 'id');
   
        // $students = Student::with('assessments','latestAssessment')->latest()->get();
        $students = Student::with('assessments.course', 'assessments.student', 'latestAssessment.course')->latest()->get();

        return view('students.index', compact('courses', 'students'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'student_no'    => 'required',
            'firstname'     => 'required',
            'middlename'    => 'required',
            'lastname'      => 'required',
            'birthdate'     => 'required',
            'address'       => 'required',
            'contact_no'    => 'required',
            'year'          => 'required',
            'email' => 'required|email|unique:users',

        ]);

        $course = Course::find(request('course'));
        $student = Student::create([
            'student_no'    => $request->student_no,
            'firstname'     => $request->firstname,
            'middlename'    => $request->middlename,
            'lastname'      => $request->lastname,
            'birthdate'     => Carbon::parse($request->birthdate),
            'address'       => request('address'),
            'contact_no'    => request('contact_no'),
            'course'        => request('course'),
            'email'         => request('email'),
        ]);

       $assessment =  $student->assessments()->create([
            'status'            => 'Verified',
            'course_id'         => $request->course,
            'semister'          => $request->semister,
            'school_year_to'    => $request->school_year_to,
            'school_year_from'  => $request->school_year_from,
            'year'              => $request->year,
            'date'              => Carbon::parse($request->date)
          ]);

        $role   = Role::where('label', 'student')->first();
        $user   = User::create([
            'username'  =>  $student->student_no,
            'email'     =>  $student->email,
            'password'  =>  bcrypt($student->student_no)
         ]);
        $user->role()->associate($role)->save();
        $user->student()->associate($student)->save();
        //Mail::to($student)->send(new SendingMail($student));
        return redirect()->route('assessments.index', $assessment)->with('success', 'Student has been added!');
    }

    public function show(Student $student)
    {
    
        $student->load(['assessments', 'latestAssessment']);
        $courses  = Course::pluck('title', 'id');

        return view('students.show', compact('student', 'courses'));
    }

    public function manageStudent(Student $student, Request $request)
    {
         $student->assessments()->create([
            'status'    => 'Verified',
            'course_id' => $request->course,
            'semister'  => $request->semister,
            'school_year_to'  => $request->school_year_to,
            'school_year_from'  => $request->school_year_from,
            'year'      => $request->year,
            'date'      => Carbon::parse($request->date)
          ]);


         return back()->with('success', 'Student has been managed!');
    }

    // public function destroy(Student $student)
    // { 
    //      $student->load('assessments'); 
    //      return back()->with('error', 'Student has been remove!');
    // }
}
