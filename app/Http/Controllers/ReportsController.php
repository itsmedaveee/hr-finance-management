<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessment;
use App\SubjectFee;
use App\Amount;
class ReportsController extends Controller
{


    public function index()
    {
        $assessments = Amount::query()->with('assessment.subjectFees');


        if (request()->has('month')) {
             $assessments->whereMonth('created_at', \Carbon\Carbon::parse(request('month'))->format('m'));
        }

        $assessments = $assessments->orderBy('created_at')->get();

      


        return view('cashier-dashboard.income-reports.index', compact('assessments'));
    }

    public function exportPdf(Assessment $assessment)
    {


        $assessments = Amount::with('assessment.subjectFees')
                                ->whereMonth('created_at',  \Carbon\Carbon::parse(request('month'))->format('m'))
                                ->orderBy('created_at')
                                ->get();



        $pdf  = \PDF::loadView('pdf.income', [
            'assessments'    => $assessments,
        ]);
        $pdf->setPaper('legal','portrait'); 
        return $pdf->stream(); 
    }



   
}
