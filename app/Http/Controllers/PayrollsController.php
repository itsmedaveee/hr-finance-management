<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payroll;
use App\Employee;
use App\User;
class PayrollsController extends Controller
{
    public function index()
    {
        $users = User::with('employee')->whereIn('role_id', [2,3])->get(); 
        
        return view('payrolls.index', compact('users'));
    }

    public function manage(\App\User $user)
    {
         $user->load('attendances');
        return view('payrolls.manage', compact('user'));
    }

    public function generate(User $user)
    {
         $pdf        = \PDF::loadView('pdf.attendance-report', [
            'user'    => $user,
        ]);

        $pdf->setPaper('legal','portrait'); 
        
        return $pdf->stream(); 
    }
}
