<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
class DashboardEmployersController extends Controller
{
    public function index()
    {
        $employee = auth()->user()->id;
        $employee = Employee::first();
        return view('employee-dashboard.index', compact('employee'));
    }

    public function generateCOE(Employee $employee)
    {
        $pdf        = \PDF::loadView('pdf.certificate-of-employee', [
            'employee'    => $employee,
        ]);

        $pdf->setPaper('legal','portrait'); 
        
        return $pdf->stream(); 
    }
}
