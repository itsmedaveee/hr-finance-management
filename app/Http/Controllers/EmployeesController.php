<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Employee;
use App\Department;
use App\Schedule;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employee::all();
        $departments = Department::all();
        $schedules = Schedule::all();
        $roles = Role::whereIn('id',[2,3])->pluck('name','id');
        return view('employees.index', compact('roles','employees', 'departments', 'schedules'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name'  => 'required',
            'email' => 'required|email|unique:users',
            'phone'  => 'required',
            'employee_no'  => 'required',
            'joining_date'  => 'required',
            'gender'  => 'required',
            'address'  => 'required',
            'birthday'  => 'required',
        ]);

    
        

        $department = Department::find(request('departments'));
        $schedule = Schedule::find($request->schedules)->first();

        $uploadId = $request->avatar;

        if (request()->hasFile('avatar')) {
            $file      = $request->file('avatar');
            $extension = $file->extension();
            $filename  = time() . '-' . str_slug($uploadId) . '.' . $extension;
            $path   = $file->storeAs('public/avatar',$filename);
        }


 
        $employee   = Employee::create([
            'name'  => request('name'),
            'phone' => request('phone'),
            'employee_no'   => request('employee_no'),
            'joining_date'  => request('joining_date'),
            'gender'  => request('gender'),
            'address'  => request('address'), 
            'rates'  => request('rates'), 
            'birthday'  => request('birthday'), 
            'positions'  => request('positions'), 
            'avatar'  =>  $path ?? null
        ]);

       $role = Role::where('name', 'Registrar')->first();
       $user = User::create([
            'employee_id' =>  $employee->id,
            'email' => request('email'),
            'username'  => request('username'),
            'password'  => bcrypt(request('password')),
        ]);

        $employee->schedules()->attach($schedule);
        $employee->department()->associate($department)->save();
        $user->role()->associate($role)->save();
        $employee->user()->associate($user)->save();
        return back()->with('success', 'Employee has been added!');
 
    }

    public function edit(Employee $employee)
    {
        $employee = $employee->load('user');
        $schedules = Schedule::all();
        
        $roles = Role::whereIn('id',[2,3])->pluck('name','id');

        $departments = Department::all();
        return view('employees.edit', compact('roles','employee', 'departments', 'schedules'));
    }

    public function update(Request $request, Employee $employee, User $user)
    {

        $employee->load('user');

        $user = $employee->user;

        $schedule = Schedule::find($request->schedules)->first();

        $uploadId = $request->avatar;

        if (request()->hasFile('avatar')) {
            $file      = $request->file('avatar');
            $extension = $file->extension();
            $filename  = time() . '-' . str_slug($uploadId) . '.' . $extension;
            $path   = $file->storeAs('public/avatar',$filename);
        }

            
       $employee->update([

                'name'  => request('name'),
                'phone' => request('phone'),
                'employee_no'   => request('employee_no'),
                'joining_date'  => request('joining_date'),
                'gender'  => request('gender'), 
                'address'  => request('address'), 
                'rates'  => request('rates'), 
                'birthday'  => request('birthday'), 
                'positions'  => request('positions'), 
                'avatar'  =>  $path ?? null
       ]);
   
        $employee->schedules()->attach($schedule);

        $department = Department::find(request('departments'));
        $role = Role::where('name', 'Registrar')->first();

        $user->update([
            'email' => request('email'),
            'employee_id' =>  $employee->id,
            'username'  => request('username'),
            'password'  => bcrypt(request('password')),
            'role_id'   => auth()->user()->id
        ]);

        $employee->department()->associate($department)->save();
        $user->role()->associate($role)->save();
        $employee->user()->associate($user)->save();

        return redirect('/employees')->with('info', 'Employee has been updated!');;

    }

    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }
    public function destroy(Employee $employee)
    {    
 
        $employee->user()->delete();
        $employee->delete();
 
        return back()->with('error', 'Employee has been removed!');;
    }

    public function enable(Employee $employee)
    {
        $employee->update([
            'status'    => 1
        ]);

        return back()->with('info', 'Certificate of Employee has enable!');
    }    

    public function disable(Employee $employee)
    {
        $employee->update([
            'status'    => 0
        ]);

        return back()->with('error', 'Certificate of Employee has disable!');
    }
}
