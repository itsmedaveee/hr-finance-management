<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leave;
use Carbon\Carbon;
class LeavesController extends Controller
{
    public function index()
    {

        $user    = auth()->user()->load('leaves');
        $leaves  = $user->leaves;
 
        return view('leaves.index', compact('leaves'));
    }

    public function store(Request $request)
    {

       $leaves =  Leave::create([
            'user_id'       =>  auth()->user()->id,
            'types_leave'   =>  $request->types_leave,
            'duration'      =>  $request->duration,
            'reason'        =>  $request->reason,
            'no_of_days'    =>  $request->no_of_days,
            'date'          =>  request('date'),
            'status'        =>  'Pending',
        ]);


        return back();
    }

    public function edit(Leave $leave)
    {
        return view('leaves.edit', compact('leave'));
    }

    public function delete(Leave $leave, Request $request)
    {
        

        $leave->delete();

        return back();
    }

    public function update(Leave $leave, Request $request)
    {
        $leave->update([

            'user_id'       =>  auth()->user()->id,
            'types_leave'   =>  $request->types_leave,
            'duration'      =>  $request->duration,
            'no_of_days'    =>  $request->no_of_days,
            'reason'        =>  $request->reason,
            'date'          =>  request('date'),
            'status'        =>  'Pending',
        ]);

        return redirect('/leaves');
    }

}
