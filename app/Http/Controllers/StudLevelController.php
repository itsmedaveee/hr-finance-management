<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudLevelController extends Controller
{
    public function store(Request $request, Student $student)
    {
        $this->validate($request, [
            'year'    => 'required',
            'course'  => 'required',
            'subject' => 'required'
        ]);

        $level = $student->levels()->create([
            'course_id' => $request->course,
            'year'      => $request->year
        ]);



        $level->subject($request->subject_id, $student->id);
        return back()->with('success', 'Year level successfully added!');

    }
}
