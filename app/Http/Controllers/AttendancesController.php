<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Attendance;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AttendanceExport;
class AttendancesController extends Controller
{
    public function index()
    { 

		$user = auth()->user()->load('attendances');
	
		$collection  = $user->attendances;

		$grouped     = $collection->groupBy('date');
		$attendances = $grouped->toArray();
 
    //  $no_of_hours = \DB::Table('attendances')
    //     ->where('user_id','=', $user->id)
    //     ->select('user_id',  \DB::raw('SEC_TO_TIME( SUM( TIME_TO_SEC( `total` ) ) ) AS total_remaining'))
    //     ->first();	
 		
    //     $remainingTime = json_encode($no_of_hours); 
     	
    	return view('attendances.index', compact('attendances'));
    }
 
    public function store(Request $request)
    {
     	 

    	if ($employee = \App\Employee::where(request('name'))->first()){
			$timeIn  = new \Carbon\Carbon($request['time_in']); 
			//$timeOut = new \Carbon\Carbon($request['time_out']); 
	    	$attendance = Attendance::create([
				'time_in'	=> $timeIn,
				'date'  => Carbon::parse($request->date)->format('Y-m-d'),
				'user_id'	=> auth()->user()->id, 
				
			]); 
	    	  if ($employee->schedules->first()->time_in >= $attendance->time_in->format('H:i:s')){
	    	  	  	$attendance->update([
	    	  	  		'status'	=> 0
	    	  	  	]);
		    	} 
           }

           return back()->with('success', 'Attendance has been added!');


    }

    public function edit(Attendance $attendance)
    {
    	return view('attendances.edit', compact('attendance'));
    }


    public function update(Request $request, Attendance $attendance)
    {


	//   $this->validate(request(),[

	//     'date'             => 'required|date',
	//     'time_in'          => 'required|date_format:H:i',
	//     'time_out'         => 'required|date_format:H:i',
	    
	// ]);

        
   	$attendance = Attendance::findOrFail(request('id'));

 	$user = auth()->user()->load('attendances');

	$timeIn  = $attendance->time_in;
	$timeOut = new \Carbon\Carbon($request['time_out']);
	$inMinutes   = $timeIn->diffInMinutes($timeOut); 
	$totalHours = $inMinutes / 60;
   $attendance->update([

			'time_out'	=> $timeOut,
			'date'  => Carbon::parse($request->date)->format('Y-m-d'),
			'user_id'	=> auth()->user()->id,
			'total'     => $totalHours

    	]);
        
   	  return back()->with('info', 'Timout');

    }


   public function showCurrent()
    {
        $timeEntry = Attendance::whereNull('time_out')
            ->whereHas('user', function ($query) {
                $query->where('id', auth()->id());
            })
            ->first();

        return response()->json(compact('timeEntry'));
    }

    public function attendanceReport()
    {

    	//$user = auth()->user()->id;

    	 $attendances = Attendance::query()->with('user');


    	if (request()->has('from') && request()->has('to')) {
            $attendances = Attendance::where('user_id', auth()->user()->id);

            $attendances = $attendances->whereBetween('date', [request('from'), request('to')])->get();
        }


		$attendances = $attendances;


    	return view('attendances.attendance-report', compact('attendances'));
    }

    public function generate()
    {
    	$user = auth()->user()->load('attendances');
	
		$collection  = $user->attendances;

		$grouped     = $collection->groupBy('date');
		$attendances = $grouped->toArray();
    	 //return view('attendances.reports.index', compact('attendances'));
    	return Excel::download(new AttendanceExport, 'report-attendance.xlsx');
    }
}
