<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
// use App\Course;
class SubjectsController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();

        // $courses = Course::pluck('title', 'id');

        return view('subjects.index', compact('subjects'));
    }

    public function store(Request $request)
    {

        $this->validate(request(), [

            'code'  => 'required',
            'name'  => 'required',
            'amount'  => 'required',
            'unit'  => 'required',
            'year'  => 'required',
        ]);

        // $course   = Course::find(request('course'));

        $subjects = Subject::create([

            'code'  => request('code'),
            'name'  => request('name'),
            'amount'    => request('amount'),
            'unit'      => request('unit'),
            'year'      => request('year'),
        ]);


       

        return back()->with('success', 'Subject has been added!');
    }

    public function edit(Subject $subject) 
    {

        return view('subjects.edit' , compact('subject'));
    }

    public function update(Subject $subject)
    {
       

        $subject->update([
            'code'  => request('code'),
            'name'  => request('name'),
            'amount'    => request('amount'),
            'unit'      => request('unit'),
            'year'      => request('year'),
        ]);


        return redirect('/subjects')->with('info', 'Subject has been updated!');
    }
}
