<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessment;
use App\Student;
use App\Subject;
use App\SubjectFee;
use Carbon\Carbon;
class AssessmentsController extends Controller
{
    public function index(Assessment $assessment)
    {
    	$assessment->load(['student']);
    	$subjects = Subject::pluck('code', 'id'); 

    	return view('assessments.index', compact('assessment', 'subjects'));
    }

    public function store(Assessment $assessment, Request $request)
    {
    	$subjects = Subject::find(request('subject'));


    	foreach ($subjects as  $subject) {

			$assessment->subjectFees()->create([

					'code'	=> $subject->code,
					'name'	=> $subject->name,
                    'unit'  => $subject->unit,
                    'date'  =>  Carbon::parse($request->date),
					'amount' => $subject->amount
			]);

    	}  


    	return back()->with('success', 'Subject has been added!');  	

    }

    public function updateStatus(Assessment $assessment)
    {
    	$assessment->update([
    		'status'	=> 'For Payment'
    	]);


    	return redirect('/students')->with('success', 'Proceed to the cashier!');
    }

    public function removedSubject(SubjectFee $subject)
    {
        $subject->delete();

        return back()->with('error', 'Subject has been removed!');
    }
}
