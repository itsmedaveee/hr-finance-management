<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
class Stats extends Model
{
	public function totalUsers()
	{
		return User::count(); 
	}

	public function totalEmployees()
	{
		return Employee::count();
	}
	public function totalSubjects()
	{
		return Subject::count();
	}	
	public function totalCourses()
	{
		return Course::count();
	}	

	public function totalStudents()
	{
		return Student::count();
	}	
	public function totalSchedules()
	{
		return Schedule::count();
	}	

	public function totalDepartments()
	{
		return Department::count();
	}
}