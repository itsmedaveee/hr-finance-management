<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Leave;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //  view()->composer('layouts.header', function ($view) {
        //     $view->with('leaves', \App\Leave::user());
        // });
    }
}
