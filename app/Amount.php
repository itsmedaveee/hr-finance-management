<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amount extends Model
{
    protected $fillable = [
    	'assessment_id',
    	'user_id',
    	'amount',
    ];

    public function assessment()
    {
    	return $this->belongsTo(Assessment::class);
    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    

}
