<?php

use Illuminate\Support\Facades\Route;

 Route::get('/attendance-report/export', 'AttendancesController@generate')->name('attendance-report/export');
 Route::get('/attendance-report/{user}', 'PayrollsController@generate');
Route::get('/generate-certificate-of-employee/{employee}', 'DashboardEmployersController@generateCOE');
Route::get('/pdf-cor-generate/{assessment}', 'PDFCORGenerateController@index');
Route::get('/settings', 'SettingsController@index');
Route::patch('/settings', 'SettingsController@update');
Route::get('/monthly-report/export', 'ReportsController@exportPdf')->name('monthly-report/export');
Route::get('/', function () {
    return view('auth.login');
});
Route::group(['middleware' => 'prevent-back-history'], function() { 


    Route::get('/', function () {
        if (auth()->check() && auth()->user()->role->name == 'Administrator') 
        {
              return view('dashboard.index');
        }

        if (auth()->check() && auth()->user()->role->name == 'Cashier')
        {
            $assessments = App\Assessment::with('student')  
                                ->where('status', 'For Payment')
                                ->orWhere('status', 'For Partial')
                                ->get();

             return view('cashier-dashboard.students-payment.index', compact('assessments'));

        }       

        if (auth()->check() && auth()->user()->role->name == 'Registrar')
        {
             return view('employee-dashboard.index');
        }

        if (auth()->check() && auth()->user()->role->name == 'Student')
        {
             $user = auth()->user()->load('student.latestAssessment');

             return view('students-dashboard.index', compact('user'));
        }

        return view('auth.login');
    }); 

    Auth::routes();
        Route::group(['middleware' => 'auth'], function(){
             Route::middleware('role:Administrator')->group(function () {
                Route::get('/home', 'HomeController@index')->name('home');

                Route::get('/admin-users', 'AdminController@index');
                Route::get('/admin-users/{admin}/edit', 'AdminController@edit');
                Route::patch('/admin/{admin}', 'AdminController@update');
                Route::get('/finance-users', 'FinancesController@index');
                Route::post('/finance', 'FinancesController@store');
                Route::post('/admin', 'AdminController@store');
                Route::resource('/employees', 'EmployeesController');
                Route::patch('/employee/{employee}', 'EmployeesController@update');
                Route::get('/employees/{employee}/show', 'EmployeesController@show');
                Route::patch('/employee/{employee}/enable-employee', 'EmployeesController@enable');
                Route::patch('/employee/{employee}/disable-employee', 'EmployeesController@disable');

                Route::get('/cashiers' , 'CashiersController@index');
                Route::post('/cashiers' , 'CashiersController@store');




                 // Route::get('students/{student}/levels/{level}/subjects', 'StudSubjectController@show')->name('students.levels.subjects');
                 // Route::patch('students/{student}/levels/{level}/subjects', 'StudSubjectController@update')->name('students.levels.subjects.update');
                 // Route::post('students/{student}/levels', 'StudLevelController@store')->name('students.levels.store');


                 
                Route::get('/departments', 'DepartmentsController@index');
                Route::post('/departments', 'DepartmentsController@store');

                Route::get('/departments/{department}/edit', 'DepartmentsController@edit');
                Route::patch('/departments/{department}', 'DepartmentsController@update');
                Route::delete('/departments/{department}', 'DepartmentsController@destroy');
                // Route::get('/reports', 'ReportsController@index');
                // Route::get('/reports/{user}', 'ReportsController@create');



                Route::get('/courses' , 'CoursesController@index');
                Route::post('/courses' , 'CoursesController@store');
                Route::get('/courses/{course}/edit' , 'CoursesController@edit');
                Route::patch('/courses/{course}' , 'CoursesController@update');
                Route::delete('/courses/{course}' , 'CoursesController@destroy');


                Route::get('/subjects', 'SubjectsController@index');
                Route::post('/subjects', 'SubjectsController@store');
                Route::get('/subjects/{subject}/edit', 'SubjectsController@edit');
                Route::patch('/subjects/{subject}', 'SubjectsController@update');

                Route::get('/schedules', 'SchedulesController@index');
                Route::post('/schedules', 'SchedulesController@store');
                Route::delete('/schedules/{schedule}', 'SchedulesController@destroy');

                Route::get('/payrolls', 'PayrollsController@index');
                Route::get('/payrolls/employees/{user}/manage', 'PayrollsController@manage');
           });

             //REGISTRAR
            Route::middleware('role:Registrar,Administrator')->group(function () { 
                Route::get('/employee/home', 'DashboardEmployersController@index');
                Route::get('/students', 'StudentsController@index');
                Route::post('/students', 'StudentsController@store');

                Route::get('/students/{student}/manage', 'StudentsController@show');
                Route::delete('/students/{student}', 'StudentsController@destroy');

                Route::post('/students/{student}/manage', 'StudentsController@manageStudent');
               


                Route::get('/assessments/{assessment}', 'AssessmentsController@index')->name('assessments.index');
                Route::post('/assessments/{assessment}', 'AssessmentsController@store');
                Route::patch('/assesments-status/{assessment}', 'AssessmentsController@updateStatus');

                Route::delete('/subject-remove/{subject}', 'AssessmentsController@removedSubject');



             });



            Route::middleware('role:Administrator,Registrar,Cashier')->group(function () { 
            Route::get('/finance/home', 'FinanceDashboardController@index');
                Route::get('/attendances', 'AttendancesController@index');
                Route::post('/attendances', 'AttendancesController@store');
                Route::get('/attendances/{attendance}/edit', 'AttendancesController@edit');
                Route::get('/attendances/show-current', 'AttendancesController@showCurrent');
                //Route::patch('/attendances/{attendance}', 'AttendancesController@update');
                Route::patch('/edit-my-attendance', 'AttendancesController@update');


                Route::get('/attendance-report', 'AttendancesController@attendanceReport');

                Route::get('/attendance-reports', 'AttendancesController@attendanceReport');
    

                Route::get('/leaves', 'LeavesController@index');
                Route::get('/leaves/{leave}/edit', 'LeavesController@edit');
                Route::post('/leaves', 'LeavesController@store');
                Route::patch('/leaves/update/{leave}', 'LeavesController@update');

                Route::delete('/leaves/{leave}', 'LeavesController@delete');

                //Pending Leaves
                Route::get('/pending-leaves', 'PendingLeavesController@pendingLeaves');
                Route::get('/manage-pending-leaves/{leave}', 'PendingLeavesController@manage');

                Route::patch('/manage-update/{leave}', 'PendingLeavesController@update');
                Route::patch('/manage-cancel/{leave}', 'PendingLeavesController@cancel');


   

             });


              Route::middleware('role:Student')->group(function () { 
                Route::get('/student/home', 'StudentDashboardController@index');
                Route::get('/student/assessment/{assessment}/subject', 'StudentDashboardController@show');
             });



            Route::middleware('role:Cashier')->group(function () { 

                Route::get('/cashier/home', 'CashiersDashboardController@index');
                Route::get('/students-payment', 'CashiersDashboardController@studentsPending');
                Route::get('/cashier/student-payment/{assessment}/manage', 'CashiersDashboardController@manageStudent');
                Route::patch('/cashier/student-payment/{assessment}', 'CashiersDashboardController@update');

                 Route::get('/students-list', 'CashiersDashboardController@studentList');
                 Route::get('/students/{assessment}/show', 'CashiersDashboardController@studentShow');
                //Finance

                Route::get('/income-report', 'ReportsController@index');

                Route::get('/transaction-logs', 'LogsController@index');
            });

    });
});
 